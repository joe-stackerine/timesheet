import React from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Login from './components/Login';
import UserCreation from './components/UserCreation';
import User from './components/User';
import Users from './components/Users';
import TimesheetCreation from './components/TimesheetCreation';
import TimesheetsUser from './components/TimesheetsUser';
import Timesheets from './components/Timesheets';
import TimesheetToComplete from './components/TimesheetToComplete';
import ProjectForm from './components/ProjectForm';
import ProjectsList from './components/ProjectsList';
import DaysOff from './components/DaysOff';
import DaysOffCreation from './components/DaysOffCreation';
import DayOff from './components/DayOff';

import Menu from './components/Menu';

import UserContextProvider from './components/UserContextProvider';

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <div>
          <UserContextProvider>
            <Route path="/" component={Menu} />
            <Route exact path="/" component={Login} />
            <Route path="/createUser" component={UserCreation} />
            <Route path="/user/:userId" component={User} />
            <Route path="/users" component={Users} />
            <Route
              exact
              path="/admin/createTimesheet"
              component={TimesheetCreation}
            />
            <Route exact path="/admin/timesheets" component={Timesheets} />
            <Route exact path="/timesheets" component={TimesheetsUser} />
            <Route
              path="/timesheet/:timesheetId"
              component={TimesheetToComplete}
            />
            <Route exact path="/admin/projects" component={ProjectsList} />
            <Route exact path="/admin/projects/form" component={ProjectForm} />
            <Route
              exact
              path="/admin/projects/form/:id"
              component={ProjectForm}
            />
            <Route exact path="/daysOff" component={DaysOff} />
            <Route exact path="/createDayOff" component={DaysOffCreation} />
            <Route path="/daysOff/:dayTimestamp" component={DayOff} />
          </UserContextProvider>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default App;
