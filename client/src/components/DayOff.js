import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DayOffForm from './DayOffForm';
import { formatDateToYYYYMMDD } from '../helpers/formatDate';
import { getDayOff, updateDayOff, deleteDayOff } from '../services/daysOff';
import withErrorManagement from '../hoc/withErrorManagement';

class DayOff extends Component {
  state = {
    type: '',
    date: '',
    isLoading: true,
  };

  componentDidMount() {
    this.getDateInfos();
  }

  getDateInfos = async () => {
    const {
      match: {
        params: { dayTimestamp },
      },
      triggerError,
    } = this.props;
    try {
      const { date, type } = await getDayOff(dayTimestamp);
      this.setState({
        type,
        date: formatDateToYYYYMMDD(date),
        isLoading: false,
      });
    } catch (e) {
      triggerError(this.getDateInfos);
    }
  };

  changeType = ({ target: { value: type } }) => this.setState({ type });

  updateDateOff = async () => {
    const { type, date } = this.state;
    const {
      history: { push },
      triggerError,
    } = this.props;
    try {
      await updateDayOff({ type, date });
      push('/daysOff');
    } catch {
      triggerError(this.updateDateOff);
    }
  };

  deleteDateOff = async () => {
    const { date } = this.state;
    const {
      history: { push },
      triggerError,
    } = this.props;
    try {
      await deleteDayOff(date);
      push('/daysOff');
    } catch {
      triggerError(this.deleteDateOff);
    }
  };

  render() {
    const { type, date, isLoading } = this.state;
    const loader = isLoading && <i className="fas fa-spinner fa-spin" />;
    const dayForm = (
      <DayOffForm
        changeType={this.changeType}
        type={type}
        date={date}
        isDisabledDate
      />
    );
    return (
      <div>
        {loader || dayForm}
        <div>
          <button onClick={this.updateDateOff} type="button">
            Save changes
          </button>
          <button onClick={this.deleteDateOff} type="button">
            Delete this day off
          </button>
        </div>
        <Link to="/daysOff">Back to see all day off</Link>
      </div>
    );
  }
}

export default withErrorManagement(DayOff);
