import React, { Fragment } from 'react';

export default function DayOffForm({
  changeType,
  type,
  date,
  isDisabledDate,
  changeInput,
}) {
  return (
    <Fragment>
      <div>
        <label>
          <span>Date :</span>
          <input
            type="date"
            value={date}
            disabled={isDisabledDate}
            onChange={changeInput}
          />
        </label>
      </div>
      <div>
        <label>Type of day off : </label>
        <select onChange={changeType.bind(this)} value={type}>
          <option value="close">close</option>
          <option value="holiday">holiday</option>
        </select>
      </div>
    </Fragment>
  );
}
