import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { formatDateToTimestamp } from '../helpers/formatDate';
import { getDaysOff } from '../services/daysOff';
import { Year, Month, Day, DayLink } from '../style/daysOff';
import withErrorManagement from '../hoc/withErrorManagement';

class DaysOff extends Component {
  state = {
    yearToShow: '',
    yearsPossibilities: [],
    year: [],
    daysOff: {},
  };

  componentDidMount = () => {
    const yearToShow = moment().year();
    const yearsPossibilities = Array(3)
      .fill(null)
      .map((year, index) => yearToShow + index);
    this.setState({
      yearToShow,
      yearsPossibilities,
    });
    this.getDaysOff();
  };

  componentDidUpdate(prevProps, prevState) {
    const { daysOff, yearToShow } = this.state;
    if (daysOff !== prevState.daysOff || yearToShow !== prevState.yearToShow) {
      this.createCalendar(yearToShow);
    }
  }

  changeYearToShow = ({ target: { value: yearToShow } }) => {
    this.setState({ yearToShow });
  };

  getDaysOff = async () => {
    const { triggerError } = this.props;
    try {
      const daysOff = await getDaysOff();
      this.setState({
        daysOff,
      });
    } catch {
      triggerError(this.getDaysOff);
    }
  };

  createCalendar = yearToShow => {
    const year = Array(12)
      .fill([])
      .map((month, indexMonth) => {
        const nbDaysInMonth = moment(
          `${yearToShow}-${indexMonth + 1}`,
          'YYYY-M'
        ).daysInMonth();
        return Array(nbDaysInMonth)
          .fill(null)
          .map((day, indexDay) => {
            const date = `${yearToShow}-${indexMonth + 1}-${indexDay + 1}`;
            return formatDateToTimestamp(date);
          });
      });
    this.setState({ year });
  };

  render() {
    const { year, daysOff, yearsPossibilities } = this.state;

    const yearSelection = (
      <select id="year" onChange={this.changeYearToShow.bind(this)}>
        {yearsPossibilities.map(yearOption => (
          <option key={yearOption} value={yearOption}>
            {yearOption}
          </option>
        ))}
      </select>
    );

    const isWE = day =>
      moment(day, 'X').day() === 6 || moment(day, 'X').day() === 0;

    const calendar = year.map((month, indexMonth) => (
      // eslint-disable-next-line react/no-array-index-key
      <Month key={indexMonth}>
        <p>{moment(indexMonth + 1, 'M').format('MMMM')}</p>
        {month.map(day => (
          <DayLink
            key={day}
            to={
              daysOff[day] ? `./daysOff/${day}` : `./createDayOff?date=${day}`
            }
          >
            <Day
              daysOff={daysOff[day] ? daysOff[day].type : null}
              weekend={isWE(day)}
            >
              {moment(day, 'X').format('DD')}
            </Day>
          </DayLink>
        ))}
      </Month>
    ));

    return (
      <div>
        <div>{yearSelection}</div>
        <Link to="./createDayOff">Add a day off</Link>
        <Year>{calendar}</Year>
      </div>
    );
  }
}

export default withErrorManagement(DaysOff);
