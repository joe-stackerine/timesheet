import React, { PureComponent } from 'react';
import qs from 'qs';
import { Link } from 'react-router-dom';
import { postDaysOff } from '../services/daysOff';
import { formatDateToYYYYMMDD } from '../helpers/formatDate';
import withErrorManagement from '../hoc/withErrorManagement';

import DayOffForm from './DayOffForm';

class DaysOffCreation extends PureComponent {
  state = {
    type: 'close',
    date: '',
  };

  componentDidMount() {
    const {
      location: { search },
    } = this.props;
    const queries = qs.parse(search.replace(/^\?/, ''));
    this.setState({ date: queries.date && formatDateToYYYYMMDD(queries.date) });
  }

  changeType = ({ target: { value: type } }) => this.setState({ type });

  changeInput = ({ target: { value } }) => this.setState({ date: value });

  createDaysOff = async () => {
    const { type, date } = this.state;
    const {
      history: { push },
      triggerError,
    } = this.props;
    try {
      await postDaysOff({ type, date });
      push('../daysOff');
    } catch {
      triggerError(this.createDaysOff);
    }
  };

  render() {
    const { date, type } = this.state;
    return (
      <div>
        <DayOffForm
          changeType={this.changeType}
          changeInput={this.changeInput}
          type={type}
          date={date}
          isDisabledDate={false}
        />
        <button type="button" onClick={this.createDaysOff}>
          Create
        </button>
        <div>
          <Link to="./daysOff">See all day off</Link>
        </div>
      </div>
    );
  }
}

export default withErrorManagement(DaysOffCreation);
