import React from 'react';
import styled from 'styled-components';

const LoadingContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const LoadingSpinner = () => {
  return (
    <LoadingContent>
      <i className="fas fa-spinner fa-spin fa-2x" />
    </LoadingContent>
  );
};

export default LoadingSpinner;
