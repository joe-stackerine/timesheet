import React, { PureComponent } from 'react';
import qs from 'qs';
import { login } from '../services/login';
import UserContext from '../context/user';
import withErrorManagement from '../hoc/withErrorManagement';

class Login extends PureComponent {
  constructor(props) {
    super(props);
    const queries = qs.parse(props.location.search.replace(/^\?/, ''));
    this.state = {
      email: '',
      password: '',
      error: null,
      redirectURL: queries.redirect,
    };
  }

  changeInput = (valueToChange, { target: { value } }) =>
    this.setState({
      [valueToChange]: value,
    });

  login = async () => {
    const { email, password, redirectURL } = this.state;
    const {
      history: { push },
      logUser,
      triggerError,
    } = this.props;
    try {
      const { err, user } = await login(email, password);
      if (!err) {
        logUser(user);
        push(redirectURL || '/users');
      } else {
        this.setState({ error: err });
      }
    } catch {
      triggerError(this.login);
    }
  };

  render() {
    const { email, password, error } = this.state;
    const errorMessage = error && <p>{error}</p>;

    return (
      <div>
        <div>
          <input
            type="email"
            placeholder="Email"
            value={email}
            onChange={this.changeInput.bind(this, 'email')}
          />
        </div>
        <div>
          <input
            type="password"
            placeholder="Password"
            value={password}
            onChange={this.changeInput.bind(this, 'password')}
          />
        </div>
        <button onClick={this.login} type="button">
          Login
        </button>
        {errorMessage}
      </div>
    );
  }
}

export default withErrorManagement(props => (
  <UserContext.Consumer>
    {({ logUser }) => <Login logUser={logUser} {...props} />}
  </UserContext.Consumer>
));
