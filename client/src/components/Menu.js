import React, { Component } from 'react';
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import withUserContext from '../hoc/withUserContext';

import { AppAndMenuContainer, AppTitle } from '../style/menu';

import AuthMenu from './menu/AuthMenu';
import NotAuthMenu from './menu/NotAuthMenu';

class Menu extends Component {
  state = {
    open: false,
  };

  toggleMenuOpen = () =>
    this.setState(prevState => ({ open: !prevState.open }));

  render() {
    const {
      user: { isAdmin, firstName: userName, id },
      history: { push },
      logoutUser,
    } = this.props;

    const { open } = this.state;

    return (
      <AppAndMenuContainer>
        <AppBar>
          <Toolbar>
            <IconButton color="inherit" onClick={this.toggleMenuOpen}>
              <i className="fas fa-bars" />
            </IconButton>
            <AppTitle>Timesheet App</AppTitle>
          </Toolbar>
        </AppBar>
        {id ? (
          <AuthMenu
            toggleMenuOpen={this.toggleMenuOpen}
            userName={userName}
            isAdmin={isAdmin}
            open={open}
            push={push}
            logoutUser={logoutUser}
          />
        ) : (
          <NotAuthMenu toggleMenuOpen={this.toggleMenuOpen} open={open} />
        )}
      </AppAndMenuContainer>
    );
  }
}

Menu.propTypes = {
  user: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  logoutUser: PropTypes.func.isRequired,
};

export default withUserContext(Menu);
