import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Row, Cell } from './projectStyle';

const ProjectElem = ({ id, projectName, society, beginIn, stopIn, users }) => {
  return (
    <Row>
      <Cell>{id}</Cell>
      <Cell>{projectName}</Cell>
      <Cell>{society}</Cell>
      <Cell>{beginIn}</Cell>
      <Cell>{stopIn}</Cell>
      <Cell>
        {Array.isArray(users) &&
          users.map(({ id: userId, name }) => <span key={userId}>{name}</span>)}
      </Cell>
      <Cell>
        <Link to={`/admin/projects/form/${id}`}>
          <i className="fas fa-edit" />
        </Link>
      </Cell>
    </Row>
  );
};

export default ProjectElem;

ProjectElem.propTypes = {
  id: PropTypes.string.isRequired,
  projectName: PropTypes.string.isRequired,
  society: PropTypes.string.isRequired,
  beginIn: PropTypes.string.isRequired,
  stopIn: PropTypes.string.isRequired,
  users: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
    })
  ),
};

ProjectElem.defaultProps = {
  users: [],
};
