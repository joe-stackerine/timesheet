import React, { Component } from 'react';
import validator from 'validator';
import PropTypes from 'prop-types';
import {
  postProject,
  putProject,
  getProjectWithUserName,
} from '../services/projects';
import { getUsers } from '../services/users';
import {
  ButtonForm,
  SelectedUser,
  SelectedUsersContainer,
  Icon,
  ErrorFormMessage,
} from './projectStyle';
import withErrorManagement from '../hoc/withErrorManagement';

const addUserToProject = (userId, userName) => ({ users }) => ({
  users: [...users, { id: userId, name: userName }],
});

class ProjectForm extends Component {
  state = {
    id: null,
    projectName: '',
    society: '',
    beginIn: '',
    stopIn: '',
    users: [],
    usersList: [],
    errors: {},
  };

  componentDidMount = () => {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    id && this.getPreviousDataProject(id);
    this.getUsersFromServer();
  };

  getPreviousDataProject = async id => {
    const { triggerError } = this.props;
    try {
      const project = await getProjectWithUserName(id);
      this.setState({ ...project });
    } catch {
      triggerError(this.getPreviousDataProject.bind(this, id));
    }
  };

  getUsersFromServer = async () => {
    const { triggerError } = this.props;
    try {
      this.setState({ usersList: await getUsers() });
    } catch {
      triggerError(this.getUsersFromServer);
    }
  };

  onChangeForm = (inputValue, { target: { value } }) => {
    this.setState({ [inputValue]: value });
  };

  checkInputValues = (...inputValues) => {
    const [projectName, society, beginIn, stopIn] = inputValues;
    const errMessage = input => `Please, enter a valid ${input}.`;
    const errProjectName = validator.isEmpty(projectName) && {
      projectName: {
        message: errMessage('project name'),
      },
    };
    const errSocietyName = validator.isEmpty(society) && {
      societyName: {
        message: errMessage('society name'),
      },
    };
    const errBeginDate = !validator.isBefore(beginIn, stopIn) && {
      beginDate: {
        message: errMessage('date (must be before end date)'),
      },
    };
    const errStopDate = !validator.isAfter(stopIn, beginIn) && {
      stopDate: {
        message: errMessage('date (must be after begin date)'),
      },
    };
    const errForm = {
      ...errProjectName,
      ...errSocietyName,
      ...errBeginDate,
      ...errStopDate,
    };

    this.setState({ errors: errForm });

    return !errProjectName && !errSocietyName && !errBeginDate && !errStopDate;
  };

  onClickValidate = async () => {
    const {
      history,
      triggerError,
      match: { params },
    } = this.props;
    const { id, projectName, society, beginIn, stopIn, users } = this.state;

    const isValidForm = this.checkInputValues(
      projectName,
      society,
      beginIn,
      stopIn
    );

    const usersId = users.map(({ id: userId }) => userId);

    if (isValidForm) {
      if (params.id) {
        try {
          await putProject({
            id: params.id,
            projectName,
            society,
            beginIn,
            stopIn,
            users: usersId,
          });
        } catch {
          return triggerError(this.onClickValidate);
        }
      } else {
        try {
          await postProject({
            id,
            projectName,
            society,
            beginIn,
            stopIn,
            users: usersId,
          });
        } catch {
          return triggerError(this.onClickValidate);
        }
      }
      return history.push('/admin/projects');
    }
    return null;
  };

  onClickReturn = () => {
    const {
      history: { push },
    } = this.props;
    push('/admin/projects');
  };

  onSelectUser = ({ target: { value: userId } }) => {
    const { usersList, users } = this.state;
    const userSelected = usersList.find(({ id }) => `${id}` === userId);
    const isAlreadySelected = users.find(({ id }) => id === userSelected.id);
    !isAlreadySelected &&
      this.setState(addUserToProject(userId, userSelected.name));
  };

  onClickRemoveUser = userId => {
    const { users } = this.state;
    const usersTab = [...users];
    const userIndex = usersTab.findIndex(({ id }) => id === userId);
    if (userIndex !== -1) {
      usersTab.splice(userIndex, 1);
      this.setState({ users: usersTab });
    }
  };

  checkInputLabelError = inputLabel => {
    const { errors } = this.state;
    return errors.find(({ inputName }) => inputName === inputLabel);
  };

  render() {
    const {
      projectName,
      society,
      beginIn,
      stopIn,
      users,
      usersList,
      errors,
    } = this.state;

    const errorDisplayed = ({ message }) => {
      return <ErrorFormMessage>{message}</ErrorFormMessage>;
    };

    return (
      <div>
        <div>
          <span>Project name : </span>
          <input
            type="text"
            onChange={this.onChangeForm.bind(this, 'projectName')}
            value={projectName}
          />
          {errors.projectName && errorDisplayed(errors.projectName)}
        </div>
        <div>
          <span>Society : </span>
          <input
            type="text"
            onChange={this.onChangeForm.bind(this, 'society')}
            value={society}
          />
          {errors.societyName && errorDisplayed(errors.societyName)}
        </div>
        <div>
          <span>Begin in : </span>
          <input
            type="date"
            onChange={this.onChangeForm.bind(this, 'beginIn')}
            value={beginIn}
          />
          {errors.beginDate && errorDisplayed(errors.beginDate)}
        </div>
        <div>
          <span>Stop in : </span>
          <input
            type="date"
            onChange={this.onChangeForm.bind(this, 'stopIn')}
            value={stopIn}
          />
          {errors.stopDate && errorDisplayed(errors.stopDate)}
        </div>
        <div>
          <span>Employees : </span>
          <select onClick={this.onSelectUser}>
            {usersList.map(({ name, id }) => (
              <option key={id} value={id}>
                {name}
              </option>
            ))}
          </select>
        </div>
        <SelectedUsersContainer>
          {Array.isArray(users) &&
            users.map(({ name, id }) => (
              <SelectedUsersSubContainer
                key={id}
                id={id}
                name={name}
                onClickRemoveUser={this.onClickRemoveUser}
              />
            ))}
        </SelectedUsersContainer>
        <ButtonForm onClick={this.onClickReturn}>Return</ButtonForm>
        <ButtonForm onClick={this.onClickValidate}>Validate</ButtonForm>
      </div>
    );
  }
}

export default withErrorManagement(ProjectForm);

const SelectedUsersSubContainer = ({ id, name, onClickRemoveUser }) => {
  const onClickIcon = () => onClickRemoveUser(id);
  return (
    <SelectedUser>
      <span>{name}</span>
      <Icon className="fas fa-user-minus" onClick={onClickIcon} />
    </SelectedUser>
  );
};

ProjectForm.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
  triggerError: PropTypes.func.isRequired,
};

ProjectForm.defaultProps = {
  match: {
    params: {
      id: null,
    },
  },
  history: {
    push: () => null,
  },
};

SelectedUsersSubContainer.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onClickRemoveUser: PropTypes.func.isRequired,
};
