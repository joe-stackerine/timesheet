import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ProjectElem from './ProjectElem';
import { getProjectsWithUserName } from '../services/projects';

import {
  HeadCell,
  Container,
  HeaderContainer,
  ButtonForm,
  ContainerProjectsList,
} from './projectStyle';
import withErrorManagement from '../hoc/withErrorManagement';

class ProjectsList extends Component {
  state = {
    projects: [],
    isLoading: true,
  };

  componentDidMount = () => {
    this.getProjectsFromServer();
  };

  getProjectsFromServer = async () => {
    const { triggerError } = this.props;
    try {
      const projects = await getProjectsWithUserName();
      this.setState({ projects, isLoading: false });
    } catch {
      triggerError(this.getProjectsFromServer);
    }
  };

  render() {
    const { projects, isLoading } = this.state;
    const loading = isLoading && <i className="fas fa-spinner" />;
    const projectsListDisplayed = projects.map(
      ({ id, projectName, society, beginIn, stopIn, users }) => (
        <ProjectElem
          key={id}
          id={id}
          projectName={projectName}
          society={society}
          beginIn={beginIn}
          stopIn={stopIn}
          users={users}
        />
      )
    );
    return (
      <ContainerProjectsList>
        <h2>Projects</h2>
        <Link to="/admin/projects/form">
          <ButtonForm>Create project</ButtonForm>
        </Link>
        <Container>
          <HeaderContainer>
            <HeadCell>ID</HeadCell>
            <HeadCell>Project Name</HeadCell>
            <HeadCell>Society</HeadCell>
            <HeadCell>Begin in</HeadCell>
            <HeadCell>Stop in</HeadCell>
            <HeadCell>Employees</HeadCell>
            <HeadCell>Actions</HeadCell>
          </HeaderContainer>
          {loading || projectsListDisplayed}
          <div />
        </Container>
      </ContainerProjectsList>
    );
  }
}

export default withErrorManagement(ProjectsList);

ProjectsList.propTypes = {
  triggerError: PropTypes.func.isRequired,
};
