import React, { Component } from 'react';
import { getTimesheet } from '../services/timesheets';
import withErrorManagement from '../hoc/withErrorManagement';
import LoadingSpinner from './LoadingSpinner';

class Timesheet extends Component {
  state = {
    timesheet: {},
    isLoading: true,
  };

  componentDidMount = () => {
    const {
      match: { params },
    } = this.props;
    const { tsId } = params;
    this.getTimesheetDatas(tsId);
  };

  getTimesheetDatas = async tsId => {
    const { triggerError } = this.props;
    try {
      const timesheet = await getTimesheet(tsId);
      this.setState({
        isLoading: false,
        timesheet,
      });
    } catch {
      triggerError(this.getTimesheetDatas.bind(this, tsId));
    }
  };

  render() {
    const { timesheet, isLoading } = this.state;
    const loading = isLoading && <LoadingSpinner />;
    const timesheetInfos = (
      <div>
        <div>
          <span>Timesheet :</span>
          {timesheet.tsId || 'No details'}
        </div>
      </div>
    );

    return loading || timesheetInfos;
  }
}

export default withErrorManagement(Timesheet);
