import React, { Component } from 'react';
import axios from 'axios';
import TimesheetPreview from './TimesheetPreview';
import { setStartMonth } from '../utils/functions';
import { formatDateToTimestamp } from '../helpers/formatDate';
import { postTimesheets } from '../services/timesheets';
import withErrorManagement from '../hoc/withErrorManagement';

class TimesheetCreation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timestamp: 0,
      timesheetPreviewDatas: {},
      timesheetExistMessage: '',
    };
  }

  onChange = ({ target: { value: date } }) => {
    const timestamp = formatDateToTimestamp(date);
    this.setState({ timestamp });
  };

  generatePreview = async () => {
    const { timestamp } = this.state;
    const { data } = await axios.post('/timesheets/timesheetPreview', {
      timestamp,
    });
    if (data.message) {
      this.setState({ timesheetExistMessage: data.message });
    } else {
      this.setState({
        timesheetPreviewDatas: data,
        timesheetExistMessage: '',
      });
    }
  };

  onPreview = async () => {
    const { triggerError } = this.props;
    const { timestamp } = this.state;
    try {
      timestamp !== 0 && (await this.generatePreview());
    } catch {
      triggerError(this.onPreview);
    }
  };

  generateTimesheets = async date => {
    const {
      history: { push },
      triggerError,
    } = this.props;
    try {
      await postTimesheets(date);
      push('/admin/timesheets');
    } catch {
      triggerError(this.generateTimesheets.bind(this, date));
    }
  };

  render() {
    const { timesheetPreviewDatas, timesheetExistMessage } = this.state;
    const preview = Object.keys(timesheetPreviewDatas).length !== 0 && (
      <TimesheetPreview
        timesheetPreviewDatas={timesheetPreviewDatas}
        generateTimesheets={this.generateTimesheets}
      />
    );
    const existMessage = timesheetExistMessage.length !== 0 && (
      <div>
        <p>{timesheetExistMessage}</p>
      </div>
    );
    const startMonth = setStartMonth();

    return (
      <div>
        <h2>Create Timesheet</h2>
        <div>
          <label>Choose a month :</label>
          <input
            type="month"
            id="timesheet-month"
            name="timesheet-month"
            min={startMonth}
            onChange={this.onChange.bind(this)}
          />
          <button onClick={this.onPreview.bind(this)} type="button">
            Preview
          </button>
        </div>
        {existMessage || preview}
      </div>
    );
  }
}

export default withErrorManagement(TimesheetCreation);
