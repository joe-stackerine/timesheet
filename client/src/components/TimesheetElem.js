import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import { formatDateToMMMMYYYY } from '../helpers/formatDate';
import { Row, Cell } from './projectStyle';

const TimesheetElem = ({
  timesheetDatas: {
    tsId,
    timestamp,
    nbFilledDays,
    nbDaysToFill,
    status,
    user: { name, firstName },
  },
}) => {
  const tsDate = formatDateToMMMMYYYY(timestamp);

  return (
    <Row key={tsId}>
      <Cell>{tsId}</Cell>
      <Cell>{tsDate}</Cell>
      <Cell>{`${name} ${firstName}`}</Cell>
      <Cell>{`${nbFilledDays} / ${nbDaysToFill}`}</Cell>
      <Cell>{status}</Cell>
      <Cell>
        <Link to={`/timesheet/${tsId}`}>
          <i className="fas fa-edit" />
        </Link>
      </Cell>
    </Row>
  );
};

export default memo(TimesheetElem);
