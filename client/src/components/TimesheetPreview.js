import React from 'react';
import moment from 'moment';
import { TimesheetContainer, Day } from '../stylized/previewStyle';

const TimesheetPreview = ({ timesheetPreviewDatas, generateTimesheets }) => {
  const { date, calendar } = timesheetPreviewDatas;
  const tsMonth = moment(`${date}`, 'X').format('MMMM YYYY');

  const preview = calendar.map(({ date: calendarDate, type }) => {
    const dayOfMonth = moment(`${calendarDate}`, 'X').format('DD');
    const isInputDisabled = type !== 'wd';
    return (
      <Day type={type} key={calendarDate}>
        <div>{dayOfMonth}</div>
        <div>
          <input type="text" disabled={isInputDisabled} />
        </div>
      </Day>
    );
  });

  return (
    <div>
      <h3>Preview</h3>
      <div>
        Timesheet of
        {tsMonth}
      </div>
      <TimesheetContainer>{preview}</TimesheetContainer>
      <div>
        <button onClick={generateTimesheets.bind(null, date)} type="button">
          Generate timesheets
        </button>
      </div>
    </div>
  );
};

export default TimesheetPreview;
