/* eslint-disable react/jsx-wrap-multilines */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  getTimesheetById,
  updateTimesheetStatus,
} from '../services/timesheets';

import {
  updateInputDayValue,
  calcSum,
  generateCalendar,
  getProjectsDetails,
  getMissingDayDetails,
  changeSavingCount,
} from '../utils/timesheetToCompleteFuncs';

import withErrorManagement from '../hoc/withErrorManagement';

import { formatDateToMMMMYYYY } from '../helpers/formatDate';

import DaysShow from './TimesheetToComplete/DaysShow';
import TimesheetContent from './TimesheetToComplete/TimesheetContent';
import TimesheetSum from './TimesheetToComplete/TimesheetSum';

import LoadingSpinner from './LoadingSpinner';

class TimesheetToComplete extends Component {
  state = {
    timesheet: {},
    calendar: [],
    timesheetDetails: [],
    sumOfWDDetails: [],
    isLoading: true,
    savingCount: 0,
    validationMessage: '',
    isValidated: false,
  };

  componentDidMount() {
    const {
      match: {
        params: { timesheetId },
      },
    } = this.props;
    this.getAllTimesheetInfos(timesheetId);
  }

  getAllTimesheetInfos = async timesheetId => {
    const { triggerError } = this.props;
    try {
      const timesheet = await getTimesheetById(timesheetId);
      const calendar = generateCalendar(timesheet);
      const timesheetDetails = [
        ...getProjectsDetails(calendar, timesheet),
        ...getMissingDayDetails(calendar, timesheet),
      ];
      const sumOfWDDetails = calcSum(timesheetDetails);
      const isValidated = timesheet.status === 'VALIDATE';
      this.setState({
        timesheet,
        calendar,
        timesheetDetails,
        sumOfWDDetails,
        isLoading: false,
        isValidated,
      });
    } catch {
      triggerError(this.getAllTimesheetInfos.bind(this, timesheetId));
    }
  };

  getSum = () => {
    const { timesheetDetails } = this.state;
    const sumOfWDDetails = calcSum(timesheetDetails);
    this.setState({ sumOfWDDetails });
  };

  changeInputTimesheet = (projectIndex, indexDay, value) => {
    this.setState(updateInputDayValue(projectIndex, indexDay, value), () => {
      this.getSum();
    });
  };

  incrementSavingCount = () => this.setState(changeSavingCount(1));

  decrementSavingCount = () => this.setState(changeSavingCount(-1));

  onValidate = async () => {
    this.setState({
      isValidated: true,
    });
    const { timesheet } = this.state;
    const { validationMessage, isValidated } = await updateTimesheetStatus(
      timesheet,
      'VALIDATE'
    );

    this.setState({
      timesheet: await getTimesheetById(timesheet.tsId),
      validationMessage,
      isValidated,
    });
  };

  onModify = async () => {
    const { timesheet } = this.state;
    const { validationMessage, isValidated } = await updateTimesheetStatus(
      timesheet,
      'IN MODIFICATION'
    );
    this.setState({
      timesheet: await getTimesheetById(timesheet.tsId),
      validationMessage,
      isValidated,
    });
  };

  render() {
    const {
      timesheetDetails,
      sumOfWDDetails,
      isLoading,
      calendar,
      savingCount,
      timesheet: { timestamp, tsId, isAdmin, status },
      validationMessage,
      isValidated,
    } = this.state;

    const loader = isLoading && <LoadingSpinner />;

    const saving = !!savingCount && <p>...saving</p>;

    const adminButton = isAdmin && (
      <div>
        <button type="button" onClick={this.onValidate} disabled={isValidated}>
          Validate
        </button>
        <button type="button" onClick={this.onModify} disabled={!isValidated}>
          Modify
        </button>
      </div>
    );

    const timesheetStatus = isAdmin && (
      <div>
        <p>
          Status:
          {status}
        </p>
      </div>
    );

    const pageContent = (
      <div>
        <h1>
          Timesheet
          {formatDateToMMMMYYYY(timestamp)}
        </h1>
        {timesheetStatus}
        <DaysShow calendar={calendar} />
        <TimesheetContent
          timesheetDetails={timesheetDetails}
          changeInputTimesheet={this.changeInputTimesheet}
          timesheetId={tsId}
          status={status}
          incrementSavingCount={this.incrementSavingCount}
          decrementSavingCount={this.decrementSavingCount}
          isValidated={isValidated}
        />
        <TimesheetSum sumOfWDDetails={sumOfWDDetails} />
        {adminButton}
        {validationMessage}
        {saving}
      </div>
    );

    return <div>{loader || pageContent}</div>;
  }
}

export default withErrorManagement(TimesheetToComplete);

TimesheetToComplete.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      timesheetId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  triggerError: PropTypes.func.isRequired,
};
