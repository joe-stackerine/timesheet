import React from 'react';
import PropTypes from 'prop-types';
import DayBox from './DayBox';
import { Month } from '../../stylized/previewStyle';

const CalendarLine = ({ datas, ...propsToPass }) => {
  const month = datas.map(({ date, type, value = '' }, indexDay) => {
    const isInputDisabled = type !== 'wd';
    return (
      <DayBox
        key={date}
        {...propsToPass}
        indexDay={indexDay}
        date={date}
        value={value}
        isInputDisabled={isInputDisabled}
        type={type}
      />
    );
  });
  return <Month>{month}</Month>;
};

CalendarLine.propTypes = {
  datas: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.string,
      type: PropTypes.string,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    })
  ).isRequired,
};

export default CalendarLine;
