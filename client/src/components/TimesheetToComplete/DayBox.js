import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { updateTimesheetDay } from '../../services/timesheets';

import { Day, InputDay } from '../../stylized/previewStyle';

import withErrorManagement from '../../hoc/withErrorManagement';

class DayBox extends Component {
  saveDayValue = async (indexDay, date, value) => {
    const {
      changeInputTimesheet,
      projectIndex,
      timesheetId,
      status,
      projectId,
      projectName,
      decrementSavingCount,
      triggerError,
    } = this.props;

    try {
      const typeDay = projectName ? 'wd' : 'md';
      const valueToSend = value || '0';
      const { value: confirmedValue } = await updateTimesheetDay(
        timesheetId,
        date,
        projectId,
        valueToSend,
        typeDay,
        status
      );
      decrementSavingCount();
      changeInputTimesheet(projectIndex, indexDay, confirmedValue);
    } catch {
      triggerError(this.saveDayValue.bind(this, indexDay, date, value));
    }
  };

  changeInputValue = async ({ target: { value } }) => {
    const {
      changeInputTimesheet,
      projectIndex,
      incrementSavingCount,
      indexDay,
      date,
    } = this.props;
    incrementSavingCount();
    changeInputTimesheet(projectIndex, indexDay, value);
    this.debouncedSaveDayData(indexDay, date, value);
  };

  debouncedSaveDayData = _.debounce(this.saveDayValue, 1000);

  render() {
    const {
      type,
      disableAll,
      isInputDisabled,
      value,
      isValidated,
    } = this.props;
    return (
      <Day type={type}>
        <InputDay
          type="number"
          disabled={isValidated || disableAll || isInputDisabled}
          value={value}
          onChange={this.changeInputValue}
          min="0"
          max="1"
        />
      </Day>
    );
  }
}

export default withErrorManagement(DayBox);

DayBox.propTypes = {
  changeInputTimesheet: PropTypes.func.isRequired,
  projectIndex: PropTypes.number,
  timesheetId: PropTypes.string,
  projectId: PropTypes.string,
  projectName: PropTypes.string,
  decrementSavingCount: PropTypes.func,
  incrementSavingCount: PropTypes.func,
  type: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  disableAll: PropTypes.bool,
  isInputDisabled: PropTypes.bool.isRequired,
  indexDay: PropTypes.number.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  isValidated: PropTypes.bool,
};

DayBox.defaultProps = {
  disableAll: false,
  projectId: null,
  projectName: null,
  projectIndex: null,
  timesheetId: null,
  decrementSavingCount: null,
  incrementSavingCount: null,
  isValidated: false,
};
