import React from 'react';

import { formatDateTodddDD } from '../../helpers/formatDate';

import {
  TimesheetContainer,
  ProjTitle,
  Day,
  Month,
} from '../../stylized/previewStyle';

const DaysShow = ({ calendar }) => (
  <TimesheetContainer>
    <ProjTitle />
    <Month>
      {calendar.map(({ date, type }) => (
        <Day key={date} type={type}>
          {formatDateTodddDD(date)}
        </Day>
      ))}
    </Month>
  </TimesheetContainer>
);

export default DaysShow;
