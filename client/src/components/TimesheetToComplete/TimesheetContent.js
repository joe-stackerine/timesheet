import React from 'react';

import CalendarLine from './CalendarLine';

import { TimesheetContainer, ProjTitle } from '../../stylized/previewStyle';

const TimesheetContent = ({ timesheetDetails, ...propsToPass }) =>
  timesheetDetails.map(({ datas, projectName, id }, projectIndex) => (
    <TimesheetContainer key={id}>
      <ProjTitle>{projectName || id}</ProjTitle>
      <CalendarLine
        datas={datas}
        projectIndex={projectIndex}
        projectId={id}
        projectName={projectName}
        {...propsToPass}
      />
    </TimesheetContainer>
  ));

export default TimesheetContent;
