import React from 'react';

import CalendarLine from './CalendarLine';
import { TimesheetContainer, ProjTitle } from '../../stylized/previewStyle';

const TimesheetSum = ({ sumOfWDDetails }) => (
  <TimesheetContainer>
    <ProjTitle>Total</ProjTitle>
    <CalendarLine
      datas={sumOfWDDetails}
      changeInputTimesheet={() => {}}
      disableAll
    />
  </TimesheetContainer>
);

export default TimesheetSum;
