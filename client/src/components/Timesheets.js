import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getTimesheetsWithUserName } from '../services/timesheets';
import LoadingSpinner from './LoadingSpinner';
import TimesheetElem from './TimesheetElem';

import {
  HeadCell,
  Container,
  HeaderContainer,
  ButtonForm,
} from './projectStyle';
import withErrorManagement from '../hoc/withErrorManagement';

class Timesheets extends Component {
  state = {
    timesheets: [],
    isLoading: true,
  };

  componentDidMount = () => {
    this.getTimesheets();
  };

  getTimesheets = async () => {
    const { triggerError } = this.props;
    try {
      const timesheets = await getTimesheetsWithUserName();
      this.setState({ timesheets, isLoading: false });
    } catch {
      triggerError(this.getTimesheets);
    }
  };

  render() {
    const { isLoading, timesheets } = this.state;
    const loading = isLoading && <LoadingSpinner />;
    const timesheetElems = timesheets.map(timesheet => (
      <TimesheetElem timesheetDatas={timesheet} />
    ));
    return (
      <div>
        <h2>Timesheets</h2>
        <Link to="/admin/createTimesheet">
          <ButtonForm>Create timesheets</ButtonForm>
        </Link>

        <Container>
          <HeaderContainer>
            <HeadCell>ID</HeadCell>
            <HeadCell>Date</HeadCell>
            <HeadCell>User</HeadCell>
            <HeadCell>Filled Days</HeadCell>
            <HeadCell>Status</HeadCell>
            <HeadCell>Actions</HeadCell>
          </HeaderContainer>
          {loading || timesheetElems}
          <div />
        </Container>
      </div>
    );
  }
}

export default withErrorManagement(Timesheets);
