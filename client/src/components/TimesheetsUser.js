import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import withAuth from '../hoc/withAuth';
import { getUserTimesheets } from '../services/timesheets';
import {
  HeadCell,
  Container,
  HeaderContainer,
  Row,
  Cell,
} from './projectStyle';
import withErrorManagement from '../hoc/withErrorManagement';

class TimesheetsUser extends Component {
  state = {
    timesheets: [],
  };

  componentDidMount() {
    const {
      user: { id: userId },
    } = this.props;
    this.getTimesheets(userId);
  }

  getTimesheets = async userId => {
    const { triggerError } = this.props;
    try {
      this.setState({
        timesheets: await getUserTimesheets(userId),
      });
    } catch {
      triggerError(this.getTimesheets.bind(this, userId));
    }
  };

  render() {
    const { timesheets } = this.state;

    const timesheetsMapped = timesheets.map(({ tsId, status, date }) => (
      <Row key={tsId}>
        <Cell>{tsId}</Cell>
        <Cell>{date}</Cell>
        <Cell>{status}</Cell>
        <Cell>
          <Link to={`/timesheet/${tsId}`}>
            <i className="fas fa-edit" />
          </Link>
        </Cell>
      </Row>
    ));

    return (
      <div>
        <h1>Timesheets</h1>

        <Container>
          <HeaderContainer>
            <HeadCell>ID</HeadCell>
            <HeadCell>Date</HeadCell>
            <HeadCell>Status</HeadCell>
            <HeadCell>Edit</HeadCell>
          </HeaderContainer>
          {timesheetsMapped}
          <div />
        </Container>
      </div>
    );
  }
}

export default withAuth(withErrorManagement(TimesheetsUser));
