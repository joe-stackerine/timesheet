import React, { PureComponent } from 'react';
import UserForm from './UserForm';
import { getUser, putUser } from '../services/users';
import UserProjects from './UserProjects';
import UserTimesheets from './UserTimesheets';
import LoadingSpinner from './LoadingSpinner';
import withErrorManagement from '../hoc/withErrorManagement';

class User extends PureComponent {
  state = {
    user: {},
    isLoading: true,
    error: null,
    disabledInputs: true,
  };

  componentDidMount = () => {
    const {
      match: {
        params: { userId },
      },
    } = this.props;
    this.getUserDatas(userId);
  };

  getUserDatas = async userId => {
    const { triggerError } = this.props;
    try {
      const user = await getUser(userId);
      this.setState({
        isLoading: false,
        user,
      });
    } catch ({ message }) {
      this.setState({
        isLoading: false,
        error: message,
      });
      triggerError(this.getUserDatas.bind(this, userId));
    }
  };

  toggleChangeDatas = () =>
    this.setState(prevState => ({ disabledInputs: !prevState.disabledInputs }));

  changeInput = (valueToChange, { target: { value } }) =>
    this.setState(prevState => ({
      user: { ...prevState.user, [valueToChange]: value },
    }));

  changeCheckbox = () =>
    this.setState(prevState => ({
      user: { ...prevState.user, isAdmin: !prevState.user.isAdmin },
    }));

  changeDatas = async () => {
    const { user } = this.state;
    const { triggerError } = this.props;
    try {
      await putUser(user.id, user);
      this.toggleChangeDatas();
    } catch {
      triggerError(this.changeDatas);
    }
  };

  render() {
    const { user, disabledInputs, isLoading, error } = this.state;
    const loading = isLoading && <LoadingSpinner />;
    const showError = error && { error };

    const form = (
      <div>
        <h3>Infos</h3>
        <button onClick={this.toggleChangeDatas} type="button">
          <i className="fas fa-edit" />
        </button>
        <button
          disabled={disabledInputs}
          onClick={this.changeDatas}
          type="button"
        >
          <i className="fas fa-save" />
        </button>
        <UserForm
          user={user}
          disabled={disabledInputs}
          changeInput={this.changeInput}
          changeCheckbox={this.changeCheckbox}
          toggleChangeDatas={this.toggleChangeDatas}
          changeDatas={this.changeDatas}
        />
        <div>
          <h3>Projects</h3>
          <UserProjects userId={user.id} userName={user.name} />
        </div>
        <div>
          <h3>Timesheets</h3>
          <UserTimesheets userId={user.id} />
        </div>
      </div>
    );

    return loading || showError || form;
  }
}

export default withErrorManagement(User);
