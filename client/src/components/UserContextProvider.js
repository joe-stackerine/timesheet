/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';
import UserContext from '../context/user';

export default class UserContextProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      logUser: this.logUser,
      logoutUser: this.logout,
    };
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      this.setState({ user });
    } else {
      this.logout();
    }
  }

  logout = () => {
    this.logUser({});
  };

  logUser = user => {
    this.setState({ user });
    localStorage.setItem('user', JSON.stringify(user));
  };

  render() {
    const { children } = this.props;
    return (
      <UserContext.Provider value={this.state}>{children}</UserContext.Provider>
    );
  }
}
