import React, { PureComponent } from 'react';
import { postUser } from '../services/users';
import UserForm from './UserForm';
import withErrorManagement from '../hoc/withErrorManagement';

class UserCreation extends PureComponent {
  state = {
    email: '',
    name: '',
    firstName: '',
    birthday: '',
    beginDate: '',
    stopDate: '',
    isAdmin: false,
  };

  changeInput = (valueToChange, { target: { value } }) =>
    this.setState({ [valueToChange]: value });

  changeCheckbox = () =>
    this.setState(prevState => ({ isAdmin: !prevState.isAdmin }));

  createUser = async () => {
    const {
      history: { push },
      triggerError,
    } = this.props;
    try {
      await postUser(this.state);
      push('/users');
    } catch {
      triggerError(this.createUser);
    }
  };

  render() {
    const {
      email,
      name,
      firstName,
      birthday,
      beginDate,
      stopDate,
      isAdmin,
    } = this.state;

    return (
      <div>
        <UserForm
          user={{
            email,
            name,
            firstName,
            birthday,
            beginDate,
            stopDate,
            isAdmin,
          }}
          disabled={false}
          changeInput={this.changeInput}
          changeCheckbox={this.changeCheckbox}
        />
        <button onClick={this.createUser} type="button">
          Create user
        </button>
      </div>
    );
  }
}

export default withErrorManagement(UserCreation);
