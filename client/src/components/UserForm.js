import React from 'react';

const UserForm = ({
  user: { email, name, firstName, birthday, beginDate, stopDate, isAdmin },
  disabled,
  changeInput,
  changeCheckbox,
}) => {
  return (
    <div>
      <p>
        Email
        <input
          type="email"
          value={email}
          disabled={disabled}
          onChange={changeInput.bind(this, 'email')}
        />
      </p>
      <p>
        Name
        <input
          type="text"
          value={name}
          disabled={disabled}
          onChange={changeInput.bind(this, 'name')}
        />
      </p>
      <p>
        First name
        <input
          type="text"
          value={firstName}
          disabled={disabled}
          onChange={changeInput.bind(this, 'firstName')}
        />
      </p>
      <p>
        Birthday
        <input
          type="date"
          value={birthday}
          disabled={disabled}
          onChange={changeInput.bind(this, 'birthday')}
        />
      </p>
      <p>
        Begin date
        <input
          type="date"
          value={beginDate}
          disabled={disabled}
          onChange={changeInput.bind(this, 'beginDate')}
        />
      </p>
      <p>
        Stop date
        <input
          type="date"
          value={stopDate}
          disabled={disabled}
          onChange={changeInput.bind(this, 'stopDate')}
        />
      </p>
      <p>
        Admin
        <input
          type="checkbox"
          checked={isAdmin}
          disabled={disabled}
          onChange={changeCheckbox}
        />
      </p>
    </div>
  );
};

export default UserForm;
