import React, { PureComponent } from 'react';
import { getProjects, putProjects } from '../services/projects';
import {
  transformProjectsArrayToObject,
  sortProjects,
} from '../utils/userProjects';
import withErrorManagement from '../hoc/withErrorManagement';

const changeUserProjects = (projectId, userId) => prevState => {
  const { projects, changedProjects } = prevState;
  const { [projectId]: projectToChange, ...otherProjects } = projects;
  const { users = [], ...otherProjInfos } = projectToChange;

  const newUsers = users.find(id => id === userId)
    ? users.filter(id => id !== userId)
    : [userId, ...users];

  const projWithNewUsers = { users: newUsers, ...otherProjInfos };

  const newChangedProjects = changedProjects.find(({ id }) => id === projectId)
    ? changedProjects.filter(({ id }) => id !== projectId)
    : [...changedProjects, { id: projectId, newUsersList: newUsers }];

  return {
    projects: { ...otherProjects, [projectId]: projWithNewUsers },
    changedProjects: newChangedProjects,
  };
};

class UserProjects extends PureComponent {
  state = {
    projects: [],
    isChangingProjects: false,
    changedProjects: [],
  };

  componentDidMount = () => {
    this.getAllProjects();
  };

  getAllProjects = async () => {
    const { triggerError } = this.props;
    try {
      const projects = transformProjectsArrayToObject(await getProjects());
      this.setState({ projects });
    } catch {
      triggerError(this.getAllProjects);
    }
  };

  changingProjects = async () => {
    const { triggerError } = this.props;
    const { isChangingProjects, changedProjects } = this.state;
    this.setState(prevState => ({
      isChangingProjects: !prevState.isChangingProjects,
    }));
    if (isChangingProjects) {
      try {
        await putProjects(changedProjects);
        this.getAllProjects();
      } catch {
        triggerError(this.changingProjects);
      }
    }
  };

  changeCheckBox = projectId => {
    const { userId } = this.props;
    this.setState(changeUserProjects(projectId, userId));
  };

  render() {
    const { projects, isChangingProjects } = this.state;
    const { userId } = this.props;

    const sortedProjects = sortProjects(projects);

    const userProjectsList = sortedProjects
      .filter(({ users }) => users.find(id => id === userId))
      .map(({ id, projectName, society }) => (
        <div key={id}>{`${projectName} - ${society}`}</div>
      ));

    const userProjectsSelectionForm = sortedProjects.map(
      ({ id, projectName, society, users = [] }) => {
        const isChecked = users.some(userIdToCheck => userIdToCheck === userId);
        return (
          <div key={id}>
            <input
              type="checkbox"
              name={id}
              checked={isChecked}
              onChange={this.changeCheckBox.bind(this, id)}
            />
            <label htmlFor={id}>{`${projectName} - ${society}`}</label>
          </div>
        );
      }
    );

    const faIcon = isChangingProjects ? 'fa-save' : 'fa-edit';
    return (
      <div>
        <button onClick={this.changingProjects} type="button">
          <i className={`fas ${faIcon}`} />
        </button>
        {isChangingProjects ? userProjectsSelectionForm : userProjectsList}
      </div>
    );
  }
}

export default withErrorManagement(UserProjects);
