import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LoadingSpinner from './LoadingSpinner';
import { getUserTimesheets } from '../services/timesheets';
import withErrorManagement from '../hoc/withErrorManagement';

class UserTimesheets extends Component {
  state = {
    timesheets: [],
    isLoading: true,
  };

  componentDidMount() {
    this.getAllUserTimesheets();
  }

  getAllUserTimesheets = async () => {
    const { triggerError, userId } = this.props;
    try {
      this.setState({
        timesheets: await getUserTimesheets(userId),
        isLoading: false,
      });
    } catch {
      triggerError(this.getAllUserTimesheets);
    }
  };

  render() {
    const { timesheets, isLoading } = this.state;
    const isPageLoading = isLoading && <LoadingSpinner />;
    const timesheetsLinks = timesheets.map(({ date, tsId, status }) => {
      return (
        <div>
          <Link to={`/timesheet/${tsId}`}>{date}</Link>
          <span>{status}</span>
        </div>
      );
    });

    return <div>{isPageLoading || timesheetsLinks}</div>;
  }
}

export default withErrorManagement(UserTimesheets);
