import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

import { getUsers } from '../services/users';
import withAuth from '../hoc/withAuth';

import {
  HeadCell,
  Container,
  HeaderContainer,
  ButtonForm,
  Row,
  Cell,
} from './projectStyle';

const getNewOrder = newOrder => prevState => {
  const { users, order, way } = prevState;

  const haveToChangeSortWay =
    newOrder !== order || (newOrder === order && way === 'decrement');

  const newWay = haveToChangeSortWay ? 'increment' : 'decrement';

  const usersOrdered =
    newWay === 'increment'
      ? users.sort(({ [newOrder]: a }, { [newOrder]: b }) => a.localeCompare(b))
      : users.reverse();

  return {
    users: usersOrdered,
    order: newOrder,
    way: newWay,
  };
};

class Users extends PureComponent {
  state = {
    users: [],
    order: null,
    way: null,
  };

  componentDidMount = () => {
    this.getUsers();
  };

  getUsers = async () => {
    const users = await getUsers();
    this.setState({ users });
  };

  orderBy = newOrder => this.setState(getNewOrder(newOrder));

  render() {
    const { users, order, way } = this.state;
    const usersMapped = users.map(
      ({ name, firstName, birthday, beginDate, stopDate, id }) => {
        return (
          <Row key={id}>
            <Cell>{id}</Cell>
            <Cell>{firstName}</Cell>
            <Cell>{name}</Cell>
            <Cell>{birthday}</Cell>
            <Cell>{beginDate}</Cell>
            <Cell>{stopDate}</Cell>
            <Cell>
              <Link to={`/user/${id}`}>
                <i className="fas fa-edit" />
              </Link>
            </Cell>
          </Row>
        );
      }
    );

    const arrow = filter => {
      if (filter === order && way === 'increment') {
        return <i className="fas fa-caret-up" />;
      }
      if (filter === order && way === 'decrement') {
        return <i className="fas fa-caret-down" />;
      }
      return null;
    };

    return (
      <div>
        <h2>Users</h2>
        <Link to="/createUser">
          <ButtonForm>Create user</ButtonForm>
        </Link>
        <Container>
          <HeaderContainer>
            <HeadCell>ID</HeadCell>
            <HeadCell onClick={this.orderBy.bind(this, 'firstName')}>
              First name
              {arrow('firstName')}
            </HeadCell>
            <HeadCell onClick={this.orderBy.bind(this, 'name')}>
              Name
              {arrow('name')}
            </HeadCell>
            <HeadCell onClick={this.orderBy.bind(this, 'birthday')}>
              Birthday
              {arrow('birthday')}
            </HeadCell>
            <HeadCell onClick={this.orderBy.bind(this, 'beginDate')}>
              Begin in
              {arrow('beginDate')}
            </HeadCell>
            <HeadCell onClick={this.orderBy.bind(this, 'stopDate')}>
              Stop in
              {arrow('stopDate')}
            </HeadCell>
            <HeadCell>Edit</HeadCell>
          </HeaderContainer>
          {usersMapped}
          <div />
        </Container>
      </div>
    );
  }
}

export default withAuth(Users);
