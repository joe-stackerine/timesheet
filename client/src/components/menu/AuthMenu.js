import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import { logout } from '../../services/login';

import { MenuContainer, WelcomeSentence } from '../../style/menu';

const AuthMenu = ({
  toggleMenuOpen,
  userName,
  isAdmin,
  open,
  push,
  logoutUser,
}) => {
  const adminMenu = [
    { text: 'Users', link: '/users' },
    { text: 'Timesheets', link: '/admin/timesheets' },
    { text: 'Projects', link: '/admin/projects' },
    { text: 'Days off', link: '/daysOff' },
  ];

  const userMenu = [{ text: 'Timesheets', link: '/timesheets' }];

  const menu = isAdmin ? adminMenu : userMenu;

  const userLogout = async () => {
    await logout();
    logoutUser({});
    push('/');
  };

  return (
    <Drawer variant="persistent" anchor="left" open={open}>
      <MenuContainer>
        <div>
          <div>
            <IconButton onClick={toggleMenuOpen}>
              <i className="fas fa-times" />
            </IconButton>
          </div>
          <WelcomeSentence>
            <span>{`Hello ${userName} !`}</span>
          </WelcomeSentence>
          <List>
            {menu.map(({ text, link }) => (
              <ListItem component={Link} to={link} button>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
        <div>
          <button type="button" onClick={userLogout}>
            Logout
          </button>
        </div>
      </MenuContainer>
    </Drawer>
  );
};

AuthMenu.propTypes = {
  toggleMenuOpen: PropTypes.func.isRequired,
  userName: PropTypes.string.isRequired,
  isAdmin: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
};

export default AuthMenu;
