import React from 'react';
import PropTypes from 'prop-types';

import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';

import { MenuContainer } from '../../style/menu';

const NotAuthMenu = ({ toggleMenuOpen, open }) => {
  return (
    <Drawer variant="persistent" anchor="left" open={open}>
      <MenuContainer>
        <div>
          <IconButton onClick={toggleMenuOpen}>
            <i className="fas fa-times" />
          </IconButton>
        </div>
        <span>You are not authenticated</span>
      </MenuContainer>
    </Drawer>
  );
};

export default NotAuthMenu;

NotAuthMenu.propTypes = {
  toggleMenuOpen: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};
