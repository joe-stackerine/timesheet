import styled from 'styled-components';

export const ContainerProjectsList = styled.div`
  width: 100%;
`;

export const Row = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const Cell = styled.div`
  flex: 1 1;
  padding: 10px;
  border: solid 1px rgba(255, 255, 255, 0.5);
`;

export const HeadCell = styled(Cell)`
  background-color: grey;
`;

export const Container = styled.div`
  background-color: #ecf0f1;
`;

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const ButtonForm = styled.button`
  margin: 10px;
  &:hover {
    cursor: pointer;
  }
`;

export const SelectedUser = styled.div`
  border: 1px solid #95a5a6;
  background-color: #ecf0f1;
  margin: 10px;
`;

export const SelectedUsersContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 50%;
  margin: 0 auto;
`;

export const Icon = styled.div`
  &:hover {
    cursor: pointer;
  }
`;

export const ErrorFormMessage = styled.div`
  color: red;
`;
