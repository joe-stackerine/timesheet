import React from 'react';

const user = {
  user: {},
  logUser: () => null,
};

export default React.createContext(user);
