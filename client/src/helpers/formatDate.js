import moment from 'moment';

export const formatDateToYYYYMMDD = timestamp =>
  moment(`${timestamp}`, 'X').format('YYYY-MM-DD');

export const formatDateToMMMMYYYY = timestamp =>
  moment(`${timestamp}`, 'X').format('MMMM YYYY');

export const formatDateTodddDD = timestamp =>
  moment(`${timestamp}`, 'X').format('ddd DD');

export const formatDateToTimestamp = date =>
  moment(`${date} +02:00`, 'YYYY-MM-DD +HH:mm').format('X');
