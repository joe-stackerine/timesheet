import React, { Component } from 'react';
import { isAuth } from '../services/login';
import LoadingSpinner from '../components/LoadingSpinner';

import UserContext from '../context/user';

const withAuth = WrappedComponent =>
  class extends Component {
    constructor(props) {
      super(props);
      const {
        location: { pathname },
      } = this.props;
      this.href = pathname;

      this.state = {
        isAuthent: false,
      };
    }

    componentDidMount() {
      this.checkIsAuth();
    }

    checkIsAuth = async () => {
      const { connected } = await isAuth();
      if (connected) {
        this.setState({
          isAuthent: connected,
        });
      } else {
        const {
          history: { push },
        } = this.props;
        push(`/?redirect=${this.href}`);
      }
    };

    render() {
      const { isAuthent } = this.state;

      return isAuthent ? (
        <UserContext.Consumer>
          {({ user }) => <WrappedComponent {...this.props} user={user} />}
        </UserContext.Consumer>
      ) : (
        <LoadingSpinner />
      );
    }
  };

export default withAuth;
