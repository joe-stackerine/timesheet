import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import {
  ErrorContainer,
  TextContainer,
  ContainerComponent,
  RetryButton,
} from '../style/errorPage';

const withErrorManagement = WrappedComponent =>
  class extends Component {
    static getDerivedStateFromError = () => ({ hasError: true });

    constructor(props) {
      super(props);
      this.state = {
        hasError: false,
        retryCallBack: null,
      };
    }

    triggerError = retryCallBack =>
      this.setState({ retryCallBack, hasError: true });

    onClickRetryButton = () => {
      const { retryCallBack } = this.state;
      this.setState({ hasError: false }, retryCallBack);
    };

    render() {
      const { hasError } = this.state;
      const displayErrorPage = (
        <ErrorContainer>
          <TextContainer>Oops, something went wrong...</TextContainer>
          <i className="far fa-grin-beam-sweat" />
          <RetryButton onClick={this.onClickRetryButton}>Retry</RetryButton>
        </ErrorContainer>
      );
      return (
        <ContainerComponent>
          <CSSTransition
            in={hasError}
            timeout={500}
            classNames="errorPage"
            unmountOnExit
            appear
          >
            {hasError ? displayErrorPage : <div />}
          </CSSTransition>
          <WrappedComponent {...this.props} triggerError={this.triggerError} />
        </ContainerComponent>
      );
    }
  };

export default withErrorManagement;
