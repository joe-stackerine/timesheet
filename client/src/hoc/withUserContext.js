import React from 'react';

import UserContext from '../context/user';

const withUserContext = WrappedComponent => props => {
  return (
    <UserContext.Consumer>
      {({ user, logoutUser }) => (
        <WrappedComponent {...props} user={user} logoutUser={logoutUser} />
      )}
    </UserContext.Consumer>
  );
};

export default withUserContext;
