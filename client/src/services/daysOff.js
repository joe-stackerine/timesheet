import axios from 'axios';
import { formatDateToTimestamp } from '../helpers/formatDate';

export const getDaysOff = async () => {
  const { data } = await axios.get('/daysOff/');
  return data;
};

export const getDayOff = async timestamp => {
  const { data } = await axios.get(`/daysOff/${timestamp}`);
  return data;
};

export const postDaysOff = async ({ type, date }) => {
  const formatedDate = formatDateToTimestamp(date);
  await axios.post('/daysOff/', { type, date: formatedDate });
};

export const updateDayOff = async ({ type, date }) => {
  const formatedDate = formatDateToTimestamp(date);
  await axios.put(`/daysOff/${formatedDate}`, { type, date: formatedDate });
};

export const deleteDayOff = async date => {
  const formatedDate = formatDateToTimestamp(date);
  await axios.delete(`/daysOff/${formatedDate}`);
};
