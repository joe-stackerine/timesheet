import axios from 'axios';

export const login = async (email, password) => {
  const { data } = await axios.post('/login', { email, password });
  return data;
};

export const isAuth = async () => {
  const { data } = await axios.get('/isAuth');
  return data;
};

export const logout = async () => {
  await axios.get('/logout');
};
