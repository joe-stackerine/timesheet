import axios from 'axios';
import {
  formatDateToYYYYMMDD,
  formatDateToTimestamp,
} from '../helpers/formatDate';

const formatProjectsList = projects => {
  const projectsTab = Object.values(projects);
  return projectsTab.map(({ beginIn, stopIn, ...others }) => {
    return {
      ...others,
      beginIn: formatDateToYYYYMMDD(beginIn),
      stopIn: formatDateToYYYYMMDD(stopIn),
    };
  });
};

export const postProject = async ({ beginIn, stopIn, ...others }) => {
  const { data: project } = await axios.post(`/projects/`, {
    ...others,
    beginIn: formatDateToTimestamp(beginIn),
    stopIn: formatDateToTimestamp(stopIn),
  });
  return project;
};

export const getProjects = async () => {
  const { data: projects } = await axios.get(`/projects/`);
  return formatProjectsList(projects);
};

export const getProjectWithUserName = async projectId => {
  const { data: project } = await axios.get(
    `/projects/${projectId}/withUserName`
  );
  return {
    ...project,
    beginIn: formatDateToYYYYMMDD(project.beginIn),
    stopIn: formatDateToYYYYMMDD(project.stopIn),
  };
};

export const getProjectsWithUserName = async () => {
  const { data: projects } = await axios.get(`/projects/withUserName`);
  return formatProjectsList(projects);
};

export const putProject = async ({ beginIn, stopIn, ...others }) => {
  const { data: project } = await axios.put(`/projects/${others.id}`, {
    ...others,
    beginIn: formatDateToTimestamp(beginIn),
    stopIn: formatDateToTimestamp(stopIn),
  });
  return project;
};

export const putProjects = async projects => axios.put('/projects/', projects);
