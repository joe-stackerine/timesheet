import axios from 'axios';
import {
  formatDateToMMMMYYYY,
  formatDateToYYYYMMDD,
} from '../helpers/formatDate';

export const postTimesheets = async date => {
  await axios.post('/timesheets/timesheetsCreation', {
    timestamp: date,
  });
};

export const getUserTimesheets = async userId => {
  const { data } = await axios.get(`/timesheets/getUserTimesheets/${userId}`);
  return data
    .sort(({ timestamp: timestampA }, { timestamp: timestampB }) =>
      timestampB.localeCompare(timestampA)
    )
    .map(({ timestamp, ...timesheet }) => ({
      date: formatDateToMMMMYYYY(timestamp),
      ...timesheet,
    }));
};

const formatReceiveTimesheetInfos = ({ timestamp, ...other }) => ({
  timestamp: formatDateToYYYYMMDD(timestamp),
  ...other,
});

export const getTimesheet = async tsId => {
  const { data } = await axios.get(`/timesheets/${tsId}`);
  const timesheetFormatedInfos = formatReceiveTimesheetInfos(data);
  return timesheetFormatedInfos;
};

export const getTimesheets = async () => {
  const { data } = await axios.get('/timesheets/');
  return data;
};

export const getTimesheetsWithUserName = async () => {
  const { data } = await axios.get('/timesheets/timesheetsWithUserName/');
  return data;
};
export const getTimesheetById = async timesheetId => {
  const { data } = await axios.get(`/timesheets/${timesheetId}`);
  return data;
};

export const updateTimesheetDay = async (
  timesheetId,
  date,
  id,
  value,
  dayType,
  status
) => {
  const { data } = await axios.post(
    `/timesheets/timesheetDays/${timesheetId}`,
    {
      date,
      id,
      value,
      dayType,
      status,
    }
  );
  return data;
};

export const updateTimesheetStatus = async (
  { status, ...otherDatas },
  newStatus
) => {
  const { data } = await axios.post(`/timesheets/${otherDatas.tsId}`, {
    ...otherDatas,
    status: newStatus,
  });
  return data;
};
