import axios from 'axios';
import {
  formatDateToYYYYMMDD,
  formatDateToTimestamp,
} from '../helpers/formatDate';

const formatReceiveUserInfos = ({
  birthday,
  beginDate,
  stopDate,
  ...otherUserInfos
}) => ({
  birthday: formatDateToYYYYMMDD(birthday),
  beginDate: formatDateToYYYYMMDD(beginDate),
  stopDate: formatDateToYYYYMMDD(stopDate),
  ...otherUserInfos,
});

const formatUserInfosToSend = ({
  birthday,
  beginDate,
  stopDate,
  ...otherUserInfos
}) => ({
  birthday: formatDateToTimestamp(birthday),
  beginDate: formatDateToTimestamp(beginDate),
  stopDate: formatDateToTimestamp(stopDate),
  ...otherUserInfos,
});

const getUser = async userId => {
  const { data } = await axios.get(`/users/${userId}`);
  const userFormatedInfos = formatReceiveUserInfos(data);
  return userFormatedInfos;
};

const getUsers = async () => {
  const { data } = await axios.get('/users/');
  const formatedUsersInfos = data.map(formatReceiveUserInfos);
  return formatedUsersInfos;
};

const postUser = async userInfos =>
  axios.post(`/users/`, formatUserInfosToSend(userInfos));

const putUser = (userId, datas) =>
  axios.put(`/users/${userId}`, formatUserInfosToSend(datas));

export { postUser, getUser, getUsers, putUser };
