import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Year = styled.div`
  display: flex;
  border: #000 1px solid;
  border-bottom: none;
`;

export const Month = styled.div`
  flex: 1;
  border-right: #000 1px solid;
  &:last-child {
    border-right: none;
  }
`;

export const Day = styled.div`
  background-color: ${({ daysOff, weekend }) => {
    const holiday = daysOff === 'holiday' && 'brown';
    const close = daysOff === 'close' && 'red';
    const we = weekend && 'grey';
    return holiday || close || we || 'white';
  }};

  border-top: #000 1px solid;
  &:last-child {
    border-bottom: #000 1px solid;
  }
  &:hover {
    background-color: #80808061;
  }
`;

export const DayLink = styled(Link)`
  text-decoration: none;
  color: #000;
  &:hover {
    font-weight: 700;
  }
`;
