import styled from 'styled-components';

export const ErrorContainer = styled.div`
  width: 30%;
  background-color: #ecf0f1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: #282828;
  position: absolute;
  padding: 20px;
  top: 100px;
  box-shadow: 0 0 5px 1px #bdc3c7;
  border-radius: 15px;
  & .far.fa-grin-beam-sweat {
    font-size: 5em;
    margin-top: 20px;
  }
  &.errorPage-enter,
  &.errorPage-appear {
    top: -500px;
  }
  &.errorPage-enter-active {
    top: 0px;
    transform: translateY(100px);
    transition: 0.5s;
  }
`;

export const TextContainer = styled.div`
  font-size: 2em;
`;

export const ContainerComponent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const RetryButton = styled.button`
  border-style: none;
  background-color: #34495e;
  color: #ecf0f1;
  padding: 10px;
  font-size: 20px;
  margin-top: 20px;
  border-radius: 10px;
  transition: 0.2s;
  &:hover {
    cursor: pointer;
    background-color: #2c3e50;
    transition: 0.2s;
  }
`;
