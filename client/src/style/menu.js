import styled from 'styled-components';

export const AppAndMenuContainer = styled.div`
  margin-top: 90px;
`;

export const MenuContainer = styled.div`
  min-width: 200px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  text-align: left;
  min-height: 100vh;
  background-color: #e2e2e2;
`;

export const AppTitle = styled.h1`
  margin-left: 20px;
`;

export const WelcomeSentence = styled.div`
  padding: 0 16px;
  margin: 20px 0;
`;
