import styled from 'styled-components';

const TimesheetContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const ProjTitle = styled.span`
  flex: 1;
`;

const Month = styled.div`
  flex: 10;
  display: flex;
  flex-wrap: nowrap;
`;

const Day = styled.div`
  flex: 1 1;
  background-color: ${props => {
    if (props.type === 'we') {
      return 'grey';
    }
    if (props.type === 'wd') {
      return 'white';
    }
    return 'pink';
  }};
`;

const InputDay = styled.input`
  background-color: ${({ value }) => (value > 1 || value < 0) && 'red'};
  width: 100%;
`;

export { TimesheetContainer, Day, InputDay, Month, ProjTitle };
