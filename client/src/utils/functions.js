import moment from 'moment';

// eslint-disable-next-line import/prefer-default-export
export const setStartMonth = () => {
  const startMonth = moment()
    .add(1, 'M')
    .format('MM');
  const startYear =
    startMonth === 0
      ? moment()
          .add(1, 'Y')
          .format('YYYY')
      : moment().format('YYYY');

  const minDate = moment(`${startYear}-${startMonth}-01`).format('YYYY-MM');
  return minDate;
};
