import { logout } from '../services/login';

// eslint-disable-next-line import/prefer-default-export
export const userLogout = async () => {
  await logout();
  localStorage.setItem('user', JSON.stringify({ id: null }));
};
