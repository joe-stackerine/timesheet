import * as timesheetToComplete from './timesheetToCompleteFuncs';

describe('updateInputDayValue', () => {
  test('should return the expected object', () => {
    expect(
      timesheetToComplete.updateInputDayValue(0, 0, 123)({
        timesheetDetails: [{ datas: [{ value: 0 }] }],
      })
    ).toEqual({ timesheetDetails: [{ datas: [{ value: 123 }] }] });
  });
});

describe('getTimestampDateFromIndex', () => {
  test('should return the expected timestamp', () => {
    expect(
      timesheetToComplete.getTimestampDateFromIndex(1, '1555079770')
    ).toEqual('1554163200');
  });
});

describe('isWeekend', () => {
  test("should return false if isn't weekend", () => {
    expect(timesheetToComplete.isWeekend('1555079770')).toBeFalsy();
  });
  test('should return true if is weekend', () => {
    expect(
      timesheetToComplete.isWeekend('1555079770', ['1555079770'])
    ).toBeTruthy();
  });
});

describe('isDayOff', () => {
  test("should return false if isn't day off", () => {
    expect(timesheetToComplete.isDayOff('1555079770')).toBeFalsy();
  });
  test('should return true if is day off', () => {
    expect(
      timesheetToComplete.isDayOff('1555079770', [{ date: '1555079770' }])
    ).toBeTruthy();
  });
});

describe('getInfos', () => {
  test('should return the expected object', () => {
    expect(
      timesheetToComplete.getInfos('1555079770', ['1555079770'], [])
    ).toEqual({ date: '1555079770', type: 'we' });
  });
  test('should return the expected object', () => {
    expect(
      timesheetToComplete.getInfos('1555079770', [], [{ date: '1555079770' }])
    ).toEqual({ date: '1555079770', type: 'dOff' });
  });
});

describe('generateCalendar', () => {
  test('should return an array with as object in as days in the timestamp month', () => {
    expect(
      timesheetToComplete.generateCalendar({
        timestamp: '1555079770',
        we: [],
        dOff: [],
      })
    ).toHaveLength(30);
  });
});

describe('isMissingDay', () => {
  test('should return true if is missing day', () => {
    expect(
      timesheetToComplete.isMissingDay(
        { 1555079770: { 123: { md: 1 } } },
        123,
        1555079770
      )
    ).toBeTruthy();
  });
  test("should return false if isn't missing day", () => {
    expect(
      timesheetToComplete.isMissingDay(
        { 1234567: { 123: { md: 1 } } },
        123,
        1555079770
      )
    ).toBeFalsy();
  });
});

describe('isWorkDay', () => {
  test('should return true if is work day', () => {
    expect(
      timesheetToComplete.isWorkDay(
        { 1555079770: { 123: { wd: 1 } } },
        123,
        1555079770
      )
    ).toBeTruthy();
  });
  test("should return false if isn't work day", () => {
    expect(
      timesheetToComplete.isWorkDay(
        { 1234567: { 123: { wd: 1 } } },
        123,
        1555079770
      )
    ).toBeFalsy();
  });
});

describe('getMoreTypeInfos', () => {
  test('should return the expected object if is workday', () => {
    expect(
      timesheetToComplete.getMoreTypeInfos(
        '1555079770',
        'wd',
        {},
        { 1555079770: { 123: { wd: 1 } } }
      )
    ).toEqual({ date: '1555079770', type: 'wd' });
  });
  test('should return the expected object if is missingday', () => {
    expect(
      timesheetToComplete.getMoreTypeInfos(
        '1555079770',
        'md',
        { 1555079770: { 123: { md: 1 } } },
        {}
      )
    ).toEqual({ date: '1555079770', type: 'md' });
  });
  test('should return the expected object if is weekend', () => {
    expect(
      timesheetToComplete.getMoreTypeInfos('1555079770', 'we', {}, {})
    ).toEqual({ date: '1555079770', type: 'we' });
  });
});

describe('generateDetailDataLine', () => {
  test('should return the correct object with value:1 if this date and project id are matching', () => {
    expect(
      timesheetToComplete.generateDetailData(
        123,
        [{ date: 1555079770, type: 'wd' }],
        { 1555079770: { 123: 1 } },
        {}
      )
    ).toEqual([{ date: 1555079770, type: 'wd', value: 1 }]);
  });

  test('should return the correct object with value:1 if date and missing id are matching', () => {
    expect(
      timesheetToComplete.generateDetailData(
        'Absence',
        [{ date: 1555079770, type: 'wd' }],
        {},
        { 1555079770: { Absence: 1 } }
      )
    ).toEqual([{ date: 1555079770, type: 'wd', value: 1 }]);
  });
  test("should return the correct object with no value if date, project or missing aren't matching", () => {
    expect(
      timesheetToComplete.generateDetailData(
        123,
        [{ date: 1555079770, type: 'wd' }],
        { 1555079770: { 456: 1 } },
        {}
      )
    ).toEqual([{ date: 1555079770, type: 'wd' }]);
  });
});

describe('getProjectsDetailData', () => {
  test('should return the correct object', () => {
    expect(
      timesheetToComplete.getProjectsDetails(
        [{ date: 1555079770, type: 'wd' }],
        {
          projects: [
            { projectName: 'Projet 1', id: 123 },
            { projectName: 'Projet 2', id: 456 },
          ],
          md: {},
          wd: { 1555079770: { 123: 1 } },
        }
      )
    ).toEqual([
      {
        datas: [{ date: 1555079770, type: 'wd', value: 1 }],
        id: 123,
        projectName: 'Projet 1',
      },
      {
        datas: [{ date: 1555079770, type: 'wd' }],
        id: 456,
        projectName: 'Projet 2',
      },
    ]);
  });
  test('should return the correct object', () => {
    expect(
      timesheetToComplete.getProjectsDetails(
        [{ date: 1555079770, type: 'wd' }],
        {
          projects: [
            { projectName: 'Projet 1', id: 123 },
            { projectName: 'Projet 2', id: 456 },
          ],
          md: {},
          wd: { 1555079770: { 456: 1 } },
        }
      )
    ).toEqual([
      {
        datas: [{ date: 1555079770, type: 'wd' }],
        id: 123,
        projectName: 'Projet 1',
      },
      {
        datas: [{ date: 1555079770, type: 'wd', value: 1 }],
        id: 456,
        projectName: 'Projet 2',
      },
    ]);
  });
  test('should return the correct object', () => {
    expect(
      timesheetToComplete.getProjectsDetails(
        [{ date: 1555079770, type: 'wd' }],
        {
          projects: [
            { projectName: 'Projet 1', id: 123 },
            { projectName: 'Projet 2', id: 456 },
          ],
          md: {},
          wd: { 1555079770: { 789: 1 } },
        }
      )
    ).toEqual([
      {
        datas: [{ date: 1555079770, type: 'wd' }],
        id: 123,
        projectName: 'Projet 1',
      },
      {
        datas: [{ date: 1555079770, type: 'wd' }],
        id: 456,
        projectName: 'Projet 2',
      },
    ]);
  });
});

describe('getMissingDayDetailData', () => {
  test('should return the correct array', () => {
    expect(
      timesheetToComplete.getMissingDayDetails(
        [{ date: 1555079770, type: 'wd' }],
        {
          projects: [{ id: 'Absence' }],
          md: { 1555079770: { Absence: 1 } },
          wd: {},
        }
      )
    ).toEqual([
      {
        datas: [{ date: 1555079770, type: 'wd' }],
        id: 'Congé',
      },
      {
        datas: [{ date: 1555079770, type: 'wd', value: 1 }],
        id: 'Absence',
      },
    ]);
  });
});

describe('calcSumDatas', () => {
  test('should return the correct object, with values added for the same date', () => {
    expect(
      timesheetToComplete.calcSum([
        {
          datas: [{ date: 1555079770, type: 'wd', value: 1 }],
          id: 123,
          projectName: 'Project 1',
        },
        {
          datas: [{ date: 1555079770, type: 'wd', value: 1 }],
          id: 456,
          projectName: 'Project 2',
        },
      ])
    ).toEqual([{ date: 1555079770, type: 'wd', value: 2 }]);
  });
  test('should return the correct object, with values added for the same date', () => {
    expect(
      timesheetToComplete.calcSum([
        {
          datas: [
            { date: 1555079770, type: 'wd', value: 1 },
            { date: 9999999999, type: 'wd', value: 0 },
          ],
          id: 123,
          projectName: 'Project 1',
        },
        {
          datas: [
            { date: 1555079770, type: 'wd', value: 0 },
            { date: 9999999999, type: 'wd', value: 1 },
          ],
          id: 456,
          projectName: 'Project 2',
        },
      ])
    ).toEqual([
      { date: 1555079770, type: 'wd', value: 1 },
      { date: 9999999999, type: 'wd', value: 1 },
    ]);
  });
});

describe('changeSavingCount', () => {
  test('should return the correct state if adding 1', () => {
    expect(
      timesheetToComplete.changeSavingCount(1)({ savingCount: 0 })
    ).toEqual({ savingCount: 1 });
  });
  test('should return the correct state if removing 1', () => {
    expect(
      timesheetToComplete.changeSavingCount(-1)({ savingCount: 3 })
    ).toEqual({ savingCount: 2 });
  });
});
