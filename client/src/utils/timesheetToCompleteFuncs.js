import moment from 'moment';

import { missingDays } from './constantes';

export const updateInputDayValue = (index, indexDay, value) => prevState => {
  const { timesheetDetails } = prevState;
  const lineToChange = timesheetDetails[index];
  const { datas: datasToChange } = lineToChange;

  const newLine = {
    ...lineToChange,
    datas: [
      ...datasToChange.slice(0, indexDay),
      { ...datasToChange[indexDay], value },
      ...datasToChange.slice(indexDay + 1),
    ],
  };
  return {
    timesheetDetails: [
      ...timesheetDetails.slice(0, index),
      newLine,
      ...timesheetDetails.slice(index + 1),
    ],
  };
};

export const getTimestampDateFromIndex = (index, timestamp) =>
  moment(timestamp, 'X')
    .startOf('month')
    .add(index, 'days')
    .add(2, 'hours')
    .format('X');

export const isWeekend = (dayToCheck, we = []) => we.includes(dayToCheck);

export const isDayOff = (dayToCheck, dOff = []) =>
  dOff.find(({ date }) => date === dayToCheck);

export const getInfos = (date, we, dOff) => {
  const type =
    (isWeekend(date, we) && 'we') || (isDayOff(date, dOff) && 'dOff') || 'wd';
  return { date, type };
};

export const generateCalendar = ({ timestamp, we, dOff }) =>
  new Array(moment(`${timestamp}`, 'X').daysInMonth())
    .fill(null)
    .map((x, index) => {
      const dayTimestamp = getTimestampDateFromIndex(index, timestamp);
      return getInfos(dayTimestamp, we, dOff);
    });

export const isMissingDay = (md, id, date) => md && md[date] && md[date][id];

export const isWorkDay = (wd, id, date) => wd && wd[date] && wd[date][id];

export const getMoreTypeInfos = (date, type, id, md, wd) => {
  if (type === 'wd' && isMissingDay(md, id, date)) {
    return { date, type, value: md[date][id] };
  }
  if (type === 'wd' && isWorkDay(wd, id, date)) {
    return { date, type, value: wd[date][id] };
  }
  return { date, type };
};

export const generateDetailData = (id, calendar, md, wd) => {
  return calendar.map(({ date, type }) =>
    getMoreTypeInfos(date, type, id, md, wd)
  );
};

export const getProjectsDetails = (calendar, { projects, md, wd }) =>
  projects.map(({ projectName, id }) => ({
    projectName,
    id,
    datas: generateDetailData(id, calendar, md, wd),
  }));

export const getMissingDayDetails = (calendar, { md, wd }) =>
  missingDays.map(missingDay => ({
    id: missingDay,
    datas: generateDetailData(missingDay, calendar, md, wd),
  }));

export const calcSum = calendar =>
  calendar[0].datas.map((day, dayIndex) =>
    calendar.reduce(
      (dayInfo, project) => {
        if (project.datas[dayIndex].value) {
          return {
            ...project.datas[dayIndex],
            value:
              Number(dayInfo.value) + Number(project.datas[dayIndex].value),
          };
        }
        return { ...project.datas[dayIndex], value: dayInfo.value || '' };
      },
      { value: 0 }
    )
  );

export const changeSavingCount = value => prevState => ({
  savingCount: prevState.savingCount + value,
});
