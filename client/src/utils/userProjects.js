export const transformProjectsArrayToObject = projects => {
  return projects.reduce((result, project) => {
    const { id } = project;
    return { ...result, [id]: { ...project } };
  }, {});
};

export const sortProjects = projects =>
  Object.values(projects).sort(
    (
      { projectName: nameA, beginIn: beginA },
      { projectName: nameB, beginIn: beginB }
    ) => {
      if (beginA === beginB) {
        return nameA.localeCompare(nameB);
      }
      return beginB.localeCompare(beginA);
    }
  );
