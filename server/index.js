const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const session = require('express-session');
const md5 = require('md5');
const redis = require('redis');
const RedisStore = require('connect-redis')(session);
const client = redis.createClient({ auth_pass: process.env.REDIS_PASSWORD });

const { findUserByEmail, findById } = require('./utils/login');

const usersRouter = require('./routers/users');
const timesheetsRouter = require('./routers/timesheets/timesheets');
const daysOffRouter = require('./routers/daysOff');
const projectsRouter = require('./routers/projects');

const app = express();
passport.use(
  'local',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, callback) => {
      const { err, user } = await findUserByEmail(email);
      if (err) {
        return callback(err);
      }
      if (!user) {
        return callback(null, false);
      }
      if (user.password !== md5(password)) {
        return callback(null, false);
      }
      return callback(null, user);
    }
  )
);

passport.serializeUser((user, callback) => {
  callback(null, user.id);
});

passport.deserializeUser(async (id, callback) => {
  const { err, user } = await findById(id);
  if (err) {
    return callback(err);
  }
  callback(null, user);
  return true;
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
  session({
    secret: 'stackerine',
    resave: false,
    saveUninitialized: false,
    store: new RedisStore({
      host: 'http://localhost',
      port: '6379',
      client: client,
      ttl: 260,
      logErrors: true,
    }),
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.post('/login', (req, res) =>
  passport.authenticate('local', (err, user) => {
    if (err) {
      console.error(err);
      res.sendStatus(500);
    }
    if (!user) {
      return res.json({ err: 'bad authentification' });
    }
    return req.login(user, error => {
      if (error) {
        console.error(error);
        res.sendStatus(500);
      }
      const { id, isAdmin, firstName } = user;
      return res.json({ user: { id, isAdmin, firstName } });
    });
  })(req, res)
);

app.get('/isAuth', (req, res) => {
  res.json(req.isAuthenticated() ? { connected: true } : { connected: false });
});

app.get('/logout', (req, res) => {
  req.logout();
  res.sendStatus(200);
});

app.use('/projects', projectsRouter);
app.use('/users', usersRouter);
app.use('/timesheets', timesheetsRouter);
app.use('/daysOff', daysOffRouter);

const server = app.listen(8080, () =>
  console.log(`Server started on http://localhost:${server.address().port}.`)
);
