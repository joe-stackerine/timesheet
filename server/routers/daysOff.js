const express = require('express');
const router = express.Router();
const { logError } = require('../services/errorsLog');

const {
  getDaysOff,
  getDayOff,
  postDaysOff,
  updateDayOff,
  deleteDayOff,
} = require('../services/daysOff');

router.get('/', async (req, res) => {
  try {
    const daysOff = await getDaysOff();
    res.json(daysOff);
  } catch (e) {
    logError(e);
  }
});

router.get('/:dayOffId', async (req, res) => {
  try {
    const { dayOffId } = req.params;
    const dayOff = await getDayOff(dayOffId);
    res.json(dayOff);
  } catch (e) {
    logError(e);
  }
});

router.post('/', async (req, res) => {
  try {
    const dayOffInfos = req.body;
    const dayOffId = dayOffInfos.date;
    await postDaysOff(dayOffId, dayOffInfos);
    res.sendStatus(201);
  } catch (e) {
    logError(e);
  }
});

router.put('/:dayOffId', async (req, res) => {
  try {
    const { dayOffId } = req.params;
    const dayOffInfos = req.body;
    await updateDayOff(dayOffId, dayOffInfos);
    res.sendStatus(204);
  } catch (e) {
    logError(e);
  }
});

router.delete('/:dayOffId', async (req, res) => {
  try {
    const { dayOffId } = req.params;
    await deleteDayOff(dayOffId);
    res.sendStatus(204);
  } catch (e) {
    logError(e);
  }
});

module.exports = router;
