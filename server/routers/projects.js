const express = require('express');
const uniqid = require('uniqid');

const { logError } = require('../services/errorsLog');

const {
  postProject,
  putProject,
  getProjects,
  getProject,
} = require('../services/projects');

const { getUser } = require('../services/users');

const router = express.Router();

router.post('/', async (req, res) => {
  const id = uniqid();
  const project = req.body;
  try {
    await postProject(id, project);
    res.sendStatus(201);
  } catch (e) {
    logError(e);
  }
});

router.get('/', async (req, res) => {
  try {
    const listProjects = await getProjects();
    res.json(listProjects);
  } catch (e) {
    logError(e);
  }
});

router.get('/:projectId/withUserName', async (req, res) => {
  const id = req.params.projectId;
  try {
    const project = await getProject(id);
    const usersWithName = await Promise.all(
      project.users.map(async user => {
        const { name } = await getUser(user);
        return { id: user, name };
      })
    );
    const projectWithUserName = project.users
      ? { ...project, users: usersWithName }
      : { ...project, users: [] };
    res.json(projectWithUserName);
  } catch (e) {
    logError(e);
  }
});

router.get('/withUserName', async (req, res) => {
  try {
    const listProjects = await getProjects();
    const projectsTab = Object.values(listProjects);
    const newTab = await Promise.all(
      projectsTab.map(async project => {
        if (project.users) {
          const userObj = await Promise.all(
            project.users.map(async user => {
              const { name } = await getUser(user);
              return { id: user, name };
            })
          );
          return { ...project, users: userObj };
        }
        return { ...project, users: [] };
      })
    );
    res.json(newTab);
  } catch (e) {
    logError(e);
  }
});

router.put('/:projectId', async (req, res) => {
  const id = req.params.projectId;
  const project = req.body;
  try {
    await putProject(id, project);
    res.sendStatus(201);
  } catch (e) {
    logError(e);
  }
});

router.put('/', async (req, res) => {
  try {
    const projects = req.body;
    await Promise.all(
      projects.map(async ({ id, newUsersList }) => {
        const { users, ...otherProjectInfo } = await getProject(id);
        const projectWithNewUsersList = {
          ...otherProjectInfo,
          users: newUsersList,
        };
        await putProject(id, projectWithNewUsersList);
      })
    );
    res.sendStatus(204);
  } catch (e) {
    logError(e);
  }
});

module.exports = router;
