const express = require('express');
const previewRouter = express.Router();
const { logError } = require('../../services/errorsLog');

const {
  setPreviewCalendar,
  verifyTimesheet,
} = require('../../utils/timesheetsFunctions');

previewRouter.post('/', async (req, res) => {
  try {
    const { timestamp } = req.body;
    const isTimesheetsExist = await verifyTimesheet(timestamp);
    if (isTimesheetsExist) {
      res.json({ message: 'Timesheets already generated for this month' });
    } else {
      const calendar = await setPreviewCalendar(timestamp);
      const timesheetPreview = { date: timestamp, calendar };
      res.json(timesheetPreview);
    }
  } catch (e) {
    logError(e);
  }
});

module.exports = previewRouter;
