const express = require('express');
const previewRouter = require('./preview');
const timesheetsCreationRouter = require('./timesheetsCreation');
const {
  getTimesheet,
  getTimesheets,
  updateTimesheetDayValue,
  getTimesheetValueDay,
  updateTimesheetStatus
} = require('../../services/timesheets');
const { getUser } = require('../../services/users');
const {
  setTimesheetWithUsersName,verifyFilledDays
} = require('../../utils/timesheetsFunctions');

const timesheetsRouter = express.Router();

const { logError } = require('../../services/errorsLog');

timesheetsRouter.get('/', async (req, res) => {
  const timesheetsInfos = Object.values(await getTimesheets());
  res.json(timesheetsInfos);
});

timesheetsRouter.use('/timesheetPreview', previewRouter);
timesheetsRouter.use('/timesheetsCreation', timesheetsCreationRouter);

timesheetsRouter.get('/timesheetsWithUserName', async (req, res) => {
  try {
    const timesheets = Object.values(await getTimesheets());
    const timesheetsWithUsers = await Promise.all(
      timesheets.map(timesheet => setTimesheetWithUsersName(timesheet))
    );
    res.json(timesheetsWithUsers);
  } catch (e) {
    console.error(e);
  }
});

timesheetsRouter.get('/:tsId', async (req, res) => {
  const tsId = req.params.tsId;
  const userId = req.session.passport.user;
  const { isAdmin } = await getUser(userId);
  const timesheetInfos = await getTimesheet(tsId);
  if (timesheetInfos) {
    return res.json({...timesheetInfos, isAdmin});
  }
  return res.sendStatus(404);
});

timesheetsRouter.use('/getUserTimesheets/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    const timesheets = await getTimesheets();
    const timesheetsFiltered = Object.values(timesheets).filter(
      ({ userId: idToFind }) => idToFind === userId
    );
    res.json(timesheetsFiltered);
  } catch (e) {
    logError(e);
  }
});

timesheetsRouter.post('/timesheetDays/:timesheetId', async (req, res) => {
  try {
    const timesheetId = req.params.timesheetId;
    const datas = req.body;
    const isValidated = datas.status === 'VALIDATE';
    if (!isValidated ){
      await updateTimesheetDayValue(timesheetId, datas);
      const { date, id, dayType } = datas;
      const returnValue = await getTimesheetValueDay(timesheetId, date, dayType)
      const valueToSend = returnValue ? returnValue[id] : '';
      res.json({ value: valueToSend });
    }
    else {
      res.json({ value: '' })
    }
  }
  catch (e) {
    logError(e);
  }
});

timesheetsRouter.get('/:timesheetId', async (req, res) => {
  try {
    const timesheetId = req.params.timesheetId;
    const timesheet = await getTimesheet(timesheetId);
    res.json(timesheet);
  } catch (e) {
    logError(e);
  }
});

timesheetsRouter.post('/:timesheetId', async (req, res) => {
  try {
    const timesheetId = req.params.timesheetId;
    const data = req.body;
    const isCompletelyFilled = verifyFilledDays(data);
    if (isCompletelyFilled){
      await updateTimesheetStatus(timesheetId, data);
      if(data.status === 'VALIDATE') {
        res.json({validationMessage: 'Timesheet validated !', isValidated: true})
      }
      else {
        res.json({validationMessage: 'Timesheet can be modified !', isValidated: false})
      }
    }
    else {
      res.json({validationMessage: 'Error: timesheet is not completely filled !', isValidated: false})
    }
    
  } catch (e) {
    logError(e);
  }
});
  
module.exports = timesheetsRouter;
