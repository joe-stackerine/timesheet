const express = require('express');
const moment = require('moment');
const timesheetsCreationRouter = express.Router();
const {
  setWeekends,
  setDaysOff,
  setUsers,
  setProjects,
  generateNewTimesheets,
} = require('../../utils/timesheetsFunctions');
const { TEMP_TIMESHEET } = require('../../utils/timesheetsConst');
const { postTimesheet } = require('../../services/timesheets');
const { logError } = require('../../services/errorsLog');

timesheetsCreationRouter.post('/', async (req, res) => {
  try {
    const { timestamp } = req.body;
    const formattedTimestamp = moment(`${timestamp}`, 'X')
      .startOf('Month')
      .add(2, 'hours')
      .format('X');
    const weekends = setWeekends(formattedTimestamp);
    const daysOff = await setDaysOff(formattedTimestamp);
    const newTempTimesheet = {
      ...TEMP_TIMESHEET,
      timestamp: formattedTimestamp,
      we: weekends,
      dOff: daysOff,
    };
    const users = await setUsers(formattedTimestamp);
    const projects = await setProjects(formattedTimestamp);
    const newTimesheets = generateNewTimesheets(
      newTempTimesheet,
      users,
      projects
    );
    newTimesheets.map(newTimesheet =>
      postTimesheet(newTimesheet.tsId, newTimesheet)
    );
    res.sendStatus(201);
  } catch (e) {
    logError(e);
  }
});

module.exports = timesheetsCreationRouter;
