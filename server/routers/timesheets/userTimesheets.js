const express = require('express');

const router = express.Router();

const { getTimesheets } = require('../../services/timesheets');

router.get('getUserTimesheets/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    const timesheets = await getTimesheets();
    const timesheetsFiltered = Object.values(timesheets).filter(
      ({ userId: idToFind }) => idToFind === userId
    );
    res.json(timesheetsFiltered);
  } catch (e) {
    console.error(e);
  }
});

module.exports = router;
