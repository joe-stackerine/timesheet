const express = require('express');
const uniqid = require('uniqid');
const md5 = require('md5');
const { logError } = require('../services/errorsLog');

const router = express.Router();

const { postUser, getUsers, getUser, putUser } = require('../services/users');

router.get('/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    const usersInfos = await getUser(userId);
    if (usersInfos) {
      const { password, ...userInfosToSend } = usersInfos;
      return res.json(userInfosToSend);
    }
    return res.sendStatus(404);
  } catch (e) {
    logError(e);
    return null;
  }
});

router.get('/', async (req, res) => {
  try {
    const usersInfos = Object.values(await getUsers());
    const usersInfosToSend = usersInfos.map(
      ({ password, ...datasToSend }) => datasToSend
    );
    res.json(usersInfosToSend);
  } catch (e) {
    logError(e);
  }
});

router.post('/', async (req, res) => {
  try {
    const userId = uniqid();
    const userInfos = req.body;
    const completeUserInfos = {
      ...userInfos,
      id: userId,
      password: md5('0000'),
    };
    await postUser(userId, completeUserInfos);
    res.sendStatus(201);
  } catch (e) {
    logError(e);
  }
});

router.put('/:userId', async (req, res) => {
  try {
    const userId = req.params.userId;
    const userInfos = req.body;
    const { password } = await getUser(userId);
    const completeUserInfos = {
      ...userInfos,
      password,
    };
    await putUser(userId, completeUserInfos);
    res.sendStatus(204);
  } catch (e) {
    logError(e);
  }
});

module.exports = router;
