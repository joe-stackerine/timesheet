const axios = require('axios');
const { JSON_STORE } = require('./jsonstore/jsonstore');
const { logError } = require('./errorsLog');

const getDaysOff = async () => {
  try {
    const {
      data: { result },
    } = await axios.get(`${JSON_STORE}/daysOff`);
    return result;
  } catch (e) {
    logError(e);
    return null;
  }
};

const getDayOff = async timestamp => {
  try {
    const {
      data: { result },
    } = await axios.get(`${JSON_STORE}/daysOff/${timestamp}`);
    return result;
  } catch (e) {
    logError(e);
    return null;
  }
};

const postDaysOff = async (dayOffId, dayOffInfos) => {
  try {
    await axios.post(`${JSON_STORE}/daysOff/${dayOffId}`, dayOffInfos, {
      headers: {
        'Content-type': 'application/json',
      },
    });
  } catch (e) {
    logError(e);
  }
};

const updateDayOff = async (dayOffId, dayOffInfos) => {
  try {
    await axios.put(`${JSON_STORE}/daysOff/${dayOffId}`, dayOffInfos);
  } catch (e) {
    logError(e);
  }
};

const deleteDayOff = async dayOffId => {
  try {
    await axios.delete(`${JSON_STORE}/daysOff/${dayOffId}`);
  } catch (e) {
    logError(e);
  }
};

module.exports = {
  getDaysOff,
  getDayOff,
  postDaysOff,
  updateDayOff,
  deleteDayOff,
};
