const {
  getDaysOff,
  getDayOff,
  postDaysOff,
  updateDayOff,
  deleteDayOff,
} = require('./daysOff');

const LogError = require('./errorsLog');

const { JSON_STORE } = require('./jsonstore/jsonstore');

const mockAxios = require('jest-mock-axios').default;

jest.mock('./errorsLog');

const spyError = jest.spyOn(LogError, 'logError');

afterEach(() => {
  mockAxios.reset();
  spyError.mockReset();
});

describe('getDaysOff', () => {
  test('should axios have been called with correct address', () => {
    getDaysOff();
    expect(mockAxios.get).toHaveBeenCalledWith(`${JSON_STORE}/daysOff`);
  });

  test('should ', async () => {
    mockAxios.get.mockReturnValue({
      data: {
        result: 'result',
      },
    });
    const daysOff = await getDaysOff();
    expect(daysOff).toEqual('result');
  });

  test('should ', async () => {
    mockAxios.get.mockImplementation(() => {
      throw new Error();
    });
    await getDaysOff();
    expect(spyError).toHaveBeenCalled();
  });
});

describe('getDayOff', () => {
  test('should axios have been called with correct address', () => {
    getDayOff(123);
    expect(mockAxios.get).toHaveBeenCalledWith(`${JSON_STORE}/daysOff/123`);
  });
  test('should ', async () => {
    mockAxios.get.mockReturnValue({
      data: {
        result: 'result',
      },
    });
    const dayOff = await getDayOff(123);
    expect(dayOff).toEqual('result');
  });
  test('should ', async () => {
    mockAxios.get.mockImplementation(() => {
      throw new Error();
    });
    await getDayOff(123);
    expect(spyError).toHaveBeenCalled();
  });
});

describe('postDayOff', () => {
  test('should axios have been called with correct address and correct arguments', () => {
    postDaysOff(123, { value: 1 });
    expect(mockAxios.post).toHaveBeenCalledWith(
      `${JSON_STORE}/daysOff/123`,
      { value: 1 },
      { headers: { 'Content-type': 'application/json' } }
    );
  });
  test('should ', async () => {
    mockAxios.post.mockImplementation(() => {
      throw new Error();
    });
    await postDaysOff();
    expect(spyError).toHaveBeenCalled();
  });
});

describe('updateDayOff', () => {
  test('should axios have been called with correct address and correct argument', () => {
    updateDayOff(123, { value: 1 });
    expect(mockAxios.put).toHaveBeenCalledWith(`${JSON_STORE}/daysOff/123`, {
      value: 1,
    });
  });
  test('should ', async () => {
    mockAxios.put.mockImplementation(() => {
      throw new Error();
    });
    await updateDayOff();
    expect(spyError).toHaveBeenCalled();
  });
});

describe('deleteDayOff', () => {
  test('should axios have been called with correct address', () => {
    deleteDayOff(123);
    expect(mockAxios.delete).toHaveBeenCalledWith(`${JSON_STORE}/daysOff/123`);
  });
  test('should ', async () => {
    mockAxios.delete.mockImplementation(() => {
      throw new Error();
    });
    await deleteDayOff();
    expect(spyError).toHaveBeenCalled();
  });
});
