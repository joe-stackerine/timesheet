const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017';
let db = null;

const initializeDatabase = async () => {
  const client = await MongoClient.connect(url, {
    auth: {
      user: process.env.MONGO_USERNAME,
      password: process.env.MONGO_PASSWORD,
    },
    poolSize: 10,
    useNewUrlParser: true,
  });
  db = client.db('timesheet');
};

const logError = async error => {
  if (process.env.NODE_ENV === 'production') {
    try {
      if (!db) {
        await initializeDatabase();
      }
      const { name, message } = error;
      await db
        .collection('errors')
        .insertOne({ name, message, date: Date.now() });
    } catch (e) {
      console.log(e);
    }
  } else {
    console.log(error);
  }
};

const getErrors = async () =>
  db
    .collection('errors')
    .find()
    .toArray();

module.exports = { getErrors, logError };
