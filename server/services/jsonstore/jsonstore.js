//  const JSON_STORE = `https://www.jsonstore.io/c86576e77b7f627671683715d46165a88fca9e3faaef5073b890eb34a84cec70`;

const JSON_STORE =
  process.env.JSON_STORE ||
  `https://www.jsonstore.io/c86576e77b7f627671683715d46165a88fca9e3faaef5073b890eb34a84cec70`;

module.exports = { JSON_STORE };
