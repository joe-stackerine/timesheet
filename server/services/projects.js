const axios = require('axios');
const { JSON_STORE } = require('./jsonstore/jsonstore');
const { formatProject } = require('../utils/projectsUtils.js');
const { logError } = require('./errorsLog');

const postProject = async (id, projectParams) => {
  try {
    const { data: project } = await axios.post(`${JSON_STORE}/projects/${id}`, {
      ...projectParams,
      id,
    });
    return project;
  } catch (e) {
    logError(e);
    return null;
  }
};

const putProject = async (id, projectParams) => {
  try {
    const { data: project } = await axios.put(
      `${JSON_STORE}/projects/${id}`,
      projectParams
    );
    return project;
  } catch (e) {
    logError(e);
    return null;
  }
};

const getProjects = async () => {
  try {
    const {
      data: { result: project },
    } = await axios.get(`${JSON_STORE}/projects`);
    return !project ? [] : project;
  } catch (e) {
    logError(e);
    return null;
  }
};

const getProject = async projectId => {
  try {
    const {
      data: { result },
    } = await axios.get(`${JSON_STORE}/projects/${projectId}`);
    return formatProject(result);
  } catch (e) {
    logError(e);
    return null;
  }
};

module.exports = { postProject, putProject, getProjects, getProject };
