const {
  postProject,
  putProject,
  getProjects,
  getProject,
} = require('./projects');
const mockAxios = require('jest-mock-axios').default;

const { JSON_STORE } = require('./jsonstore/jsonstore');

const projectData = {
  id: '3g0rgeqjthg5alt',
  projectName: 'Project1',
  societyName: 'Society',
  beginIn: '2019-03-02',
  stopIn: '2019-03-17',
  employees: {},
};

afterEach(() => {
  mockAxios.reset();
});

describe('Test getProject :  get a project.', () => {
  test('Should call axios.', () => {
    getProject('3g0rgeqjthg5alt');
    expect(mockAxios.get).toHaveBeenCalled();
  });
  test('Should call axios with correct url.', () => {
    getProject('3g0rgeqjthg5alt');
    expect(mockAxios.get).toHaveBeenCalledWith(
      `${JSON_STORE}/projects/3g0rgeqjthg5alt`
    );
  });
});

describe('Test getProjects : get all projects.', () => {
  test('Should call axios.', () => {
    getProjects();
    expect(mockAxios.get).toHaveBeenCalled();
  });
  test('Should call axios with correct url', () => {
    getProjects();
    expect(mockAxios.get).toHaveBeenCalledWith(`${JSON_STORE}/projects`);
  });
});

describe('Test postProject : create a project.', () => {
  test('Should call axios with correct URL and parameters.', () => {
    postProject('3g0rgeqjthg5alt', projectData);
    expect(mockAxios.post).toHaveBeenCalled();
  });
  test('Should call axios with correct url and parameters.', () => {
    postProject('3g0rgeqjthg5alt', projectData);
    expect(mockAxios.post).toHaveBeenCalledWith(
      `${JSON_STORE}/projects/3g0rgeqjthg5alt`,
      projectData
    );
  });
});

describe('Test putProject : update a project.', () => {
  test('Should call axios.', () => {
    putProject('3g0rgeqjthg5alt', projectData);
    expect(mockAxios.put).toHaveBeenCalled();
  });
  test('Should call axios with correct url and parameters.', () => {
    putProject('3g0rgeqjthg5alt', projectData);
    expect(mockAxios.put).toHaveBeenCalledWith(
      `${JSON_STORE}/projects/3g0rgeqjthg5alt`,
      projectData
    );
  });
});
