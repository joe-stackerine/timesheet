const axios = require('axios');
const { JSON_STORE } = require('./jsonstore/jsonstore');
const defaultDatas = require('../utils/clearDBDatas');
const { logError } = require('./errorsLog');

const clearDB = async () => {
  try {
    await axios.delete(`${JSON_STORE}/`);
  } catch (e) {
    logError(e);
  }
};

const dbInitialise = async () => {
  try {
    await axios.post(`${JSON_STORE}/`, defaultDatas);
  } catch (e) {
    logError(e);
  }
};
const resetDb = async () => {
  try {
    await clearDB();
    await dbInitialise();
  } catch (e) {
    logError(e);
  }
};

resetDb();
