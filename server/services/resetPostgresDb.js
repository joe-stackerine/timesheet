const { Pool } = require('pg');
const {
  usersData,
  projectsData,
  timesheetsData,
  daysOffData,
} = require('../utils/basePostgresData');

const pool = new Pool({
  user: 'admin',
  host: 'localhost',
  database: 'mydb',
  password: 'admin',
  port: 5432,
});

const tables = ['users', 'projects', 'timesheets', 'daysoff'];

const dropTables = async () => {
  try {
    await Promise.all(
      tables.map(async table => {
        await pool.query(`DROP TABLE ${table}`);
      })
    );
  } catch (e) {
    console.error(e);
  }
};

const createTables = async () => {
  try {
    await pool.query(
      `CREATE TABLE users (id BIGSERIAL, isadmin BOOLEAN, name TEXT, firstname TEXT, email TEXT, password TEXT, birthday INTEGER, begindate INTEGER, stopdate INTEGER);`
    );
  } catch (e) {
    console.error(e);
  }
  try {
    await pool.query(
      `CREATE TABLE projects (id BIGSERIAL, projectname TEXT, society TEXT, beginin INTEGER, stopin INTEGER, usersid INTEGER[] );`
    );
  } catch (e) {
    console.error(e);
  }
  try {
    await pool.query(
      `CREATE TABLE timesheets (id BIGSERIAL, status TEXT, timestamp INTEGER, userid INTEGER, weekends INTEGER[]);`
    );
  } catch (e) {
    console.error(e);
  }
  try {
    await pool.query(
      `CREATE TABLE daysOff (id BIGSERIAL, date INTEGER, type TEXT);`
    );
  } catch (e) {
    console.error(e);
  }
};

const fillTable = async tabData => {
  const { text, values1, values2, values3 } = tabData;
  try {
    await pool.query(text, values1);
  } catch (e) {
    console.error(e);
  }
  try {
    await pool.query(text, values2);
  } catch (e) {
    console.error(e);
  }
  try {
    await pool.query(text, values3);
  } catch (e) {
    console.error(e);
  }
};

const fillTables = async () => {
  await fillTable(usersData);
  await fillTable(projectsData);
  await fillTable(timesheetsData);
  await fillTable(daysOffData);
};

const initDb = async () => {
  await dropTables();
  await createTables();
  await fillTables();
};

initDb();
