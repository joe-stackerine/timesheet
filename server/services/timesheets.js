const axios = require('axios');
const { JSON_STORE } = require('./jsonstore/jsonstore');
const { logError } = require('./errorsLog');

const postTimesheet = async (tsId, datas) => {
  try {
    await axios.post(`${JSON_STORE}/timesheets/${tsId}`, datas, {
      headers: {
        'Content-type': 'application/json',
      },
    });
  } catch (e) {
    logError(e);
  }
};

const getTimesheet = async tsId => {
  const {
    data: { result },
  } = await axios.get(`${JSON_STORE}/timesheets/${tsId}`);
  return result;
};

const getTimesheets = async () => {
  const {
    data: { result },
  } = await axios.get(`${JSON_STORE}/timesheets`);
  return result;
};

const updateTimesheetDayValue = async (
  timesheetId,
  { date, id, value, dayType }
) => {
  await axios.post(
    `${JSON_STORE}/timesheets/${timesheetId}/${dayType}/${date}/${id}`,
    value,
    {
      headers: {
        'Content-type': 'application/json',
      },
    }
  );
};

const getTimesheetValueDay = async (timesheetId, date, dayType) => {
  const {
    data: { result },
  } = await axios.get(
    `${JSON_STORE}/timesheets/${timesheetId}/${dayType}/${date}`
  );
  return result;
};

const updateTimesheetStatus = async (
  timesheetId, status
) => {
  await axios.post(
    `${JSON_STORE}/timesheets/${timesheetId}/`,
    status,
    {
      headers: {
        'Content-type': 'application/json',
      },
    }
  );
};

module.exports = {
  getTimesheets,
  postTimesheet,
  getTimesheet,
  getTimesheetValueDay,
  updateTimesheetDayValue,
  updateTimesheetStatus
};
