const axios = require('axios');
const { JSON_STORE } = require('./jsonstore/jsonstore');
const { logError } = require('./errorsLog');

const postUser = async (userID, datas) => {
  try {
    await axios.post(`${JSON_STORE}/users/${userID}`, datas, {
      headers: {
        'Content-type': 'application/json',
      },
    });
  } catch (e) {
    logError(e);
  }
};

const getUser = async userID => {
  try {
    const {
      data: { result },
    } = await axios.get(`${JSON_STORE}/users/${userID}`);
    return result;
  } catch (e) {
    logError(e);
    return null;
  }
};

const getUsers = async () => {
  try {
    const {
      data: { result },
    } = await axios.get(`${JSON_STORE}/users`);
    return result;
  } catch (e) {
    logError(e);
    return null;
  }
};

const putUser = async (userID, newDatas) => {
  try {
    await axios.put(`${JSON_STORE}/users/${userID}`, newDatas);
  } catch (e) {
    logError(e);
  }
};

module.exports = { postUser, getUsers, getUser, putUser };
