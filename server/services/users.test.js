const { postUser, getUsers, getUser, putUser } = require('./users');
const mockAxios = require('jest-mock-axios').default;

const { JSON_STORE } = require('./jsonstore/jsonstore');

afterEach(() => {
  mockAxios.reset();
});

describe('getUser', () => {
  test('should have been called', () => {
    getUsers();
    expect(mockAxios.get).toHaveBeenCalled();
  });
  test('should have been called with correct url', () => {
    getUsers();
    expect(mockAxios.get).toHaveBeenCalledWith(`${JSON_STORE}/users`);
  });
});

describe('getUser', () => {
  test('should have been called', () => {
    getUser(123);
    expect(mockAxios.get).toHaveBeenCalled();
  });
  test('should have been called with correct url', () => {
    getUser(123);
    expect(mockAxios.get).toHaveBeenCalledWith(`${JSON_STORE}/users/123`);
  });
});

describe('postUser', () => {
  test('should have been called', () => {
    postUser(123, { name: 'Toto' });
    expect(mockAxios.post).toHaveBeenCalled();
  });
  test('should have been called with correct url and props', () => {
    postUser(123, { name: 'Toto' });
    expect(mockAxios.post).toHaveBeenCalledWith(
      `${JSON_STORE}/users/123`,
      { name: 'Toto' },
      { headers: { 'Content-type': 'application/json' } }
    );
  });
});

describe('putUser', () => {
  test('should have been called', () => {
    putUser(123, { name: 'Toto' });
    expect(mockAxios.put).toHaveBeenCalled();
  });
  test('should have been called with correct url and props', () => {
    putUser(123, { name: 'Toto' });
    expect(mockAxios.put).toHaveBeenCalledWith(`${JSON_STORE}/users/123`, {
      name: 'Toto',
    });
  });
});
