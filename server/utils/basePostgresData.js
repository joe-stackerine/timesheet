const md5 = require('md5');

const weList = [
  '1556928000',
  '1557014400',
  '1557532800',
  '1557619200',
  '1558137600',
  '1558224000',
  '1558742400',
  '1558828800',
];

const usersData = {
  text: `INSERT INTO users (id, isadmin, name, firstname, email, password, birthday, begindate, stopdate) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *`,
  values1: [
    0,
    false,
    'Aude',
    'Javel',
    'aude.javel@gmail.com',
    md5('vivelajavel'),
    978310800,
    1546304400,
    1576112400,
  ],
  values2: [
    1,
    false,
    'Debby',
    'Scotto',
    'debby.scotto@gmail.com',
    md5('vivelesbiscottos'),
    978310800,
    1546304400,
    1576112400,
  ],
  values3: [
    2,
    false,
    'Axelle',
    'Aire',
    'axelle.aire@gmail.com',
    md5('vivelacceleration'),
    978310800,
    1546304400,
    1576112400,
  ],
};

const projectsData = {
  text: `INSERT INTO projects (id, projectname, society, beginin, stopin, usersid) VALUES($1, $2, $3, $4, $5, $6) RETURNING *`,
  values1: [0, 'Project1', 'Society1', 1546304400, 1576112400, [0, 1]],
  values2: [1, 'Project2', 'Society2', 1546304400, 1576112400, [1]],
  values3: [2, 'Project3', 'Society3', 1546304400, 1576112400, [1, 2]],
};

const timesheetsData = {
  text: `INSERT INTO timesheets (id, status, timestamp, userid, weekends) VALUES($1, $2, $3, $4, $5) RETURNING *`,
  values1: [0, 'TODO', 1556668800, 0, weList],
  values2: [1, 'TODO', 1556668800, 1, weList],
  values3: [2, 'TODO', 1556668800, 2, weList],
};

const daysOffData = {
  text: `INSERT INTO daysOff (id, date, type) VALUES($1, $2, $3) RETURNING *`,
  values1: [0, 1552006800, 'holiday'],
  values2: [1, 1555286400, 'close'],
  values3: [2, 1556841600, 'close'],
};

module.exports = { usersData, projectsData, timesheetsData, daysOffData };
