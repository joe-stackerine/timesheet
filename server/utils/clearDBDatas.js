const defaultDatas = {
  daysOff: {
    '1552006800': {
      date: '1552006800',
      type: 'holiday',
    },
    '1555286400': {
      date: '1555286400',
      type: 'holiday',
    },
    '1556841600': {
      date: '1556841600',
      type: 'close',
    },
    '1557446400': {
      date: '1557446400',
      type: 'holiday',
    },
    '1558483200': {
      date: '1558483200',
      type: 'close',
    },
  },
  projects: {
    afqlcadju46de4n: {
      beginIn: '1546304400',
      id: 'afqlcadju46de4n',
      projectName: 'Project 1',
      society: 'Stackerine',
      stopIn: '1577754000',
      users: ['4g42mol1jtfutdaa', 'z9001bu6sbjtu4er7r'],
    },
    afqlcadju46dtoz: {
      beginIn: '1546304400',
      id: 'afqlcadju46dtoz',
      projectName: 'Projet 2',
      society: 'Stackerine',
      stopIn: '1672448400',
      users: ['z9001bu6sbjtu4er7r'],
    },
  },
  timesheets: {
    afqlcadju46gb22: {
      dOff: [
        {
          date: '1556841600',
          type: 'close',
        },
        {
          date: '1557446400',
          type: 'holiday',
        },
        {
          date: '1558483200',
          type: 'close',
        },
      ],
      projects: [
        {
          id: 'afqlcadju46de4n',
          projectName: 'Project 1',
        },
      ],
      status: 'TODO',
      timestamp: '1556668800',
      tsId: 'afqlcadju46gb22',
      userId: '4g42mol1jtfutdaa',
      we: [
        '1556928000',
        '1557014400',
        '1557532800',
        '1557619200',
        '1558137600',
        '1558224000',
        '1558742400',
        '1558828800',
      ],
    },
    afqlcadju46gb23: {
      dOff: [
        {
          date: '1556841600',
          type: 'close',
        },
        {
          date: '1557446400',
          type: 'holiday',
        },
        {
          date: '1558483200',
          type: 'close',
        },
      ],
      projects: [
        {
          id: 'afqlcadju46de4n',
          projectName: 'Project 1',
        },
        {
          id: 'afqlcadju46dtoz',
          projectName: 'Projet 2',
        },
      ],
      status: 'TODO',
      timestamp: '1556668800',
      tsId: 'afqlcadju46gb23',
      userId: 'z9001bu6sbjtu4er7r',
      we: [
        '1556928000',
        '1557014400',
        '1557532800',
        '1557619200',
        '1558137600',
        '1558224000',
        '1558742400',
        '1558828800',
      ],
    },
    afqlcadju46gn5a: {
      projects: [
        {
          id: 'afqlcadju46de4n',
          projectName: 'Project 1',
        },
      ],
      status: 'TODO',
      timestamp: '1559347200',
      tsId: 'afqlcadju46gn5a',
      userId: '4g42mol1jtfutdaa',
      we: [
        '1559347200',
        '1559433600',
        '1559952000',
        '1560038400',
        '1560556800',
        '1560643200',
        '1561161600',
        '1561248000',
        '1561766400',
        '1561852800',
      ],
    },
    afqlcadju46gn5b: {
      projects: [
        {
          id: 'afqlcadju46de4n',
          projectName: 'Project 1',
        },
        {
          id: 'afqlcadju46dtoz',
          projectName: 'Projet 2',
        },
      ],
      status: 'TODO',
      timestamp: '1559347200',
      tsId: 'afqlcadju46gn5b',
      userId: 'z9001bu6sbjtu4er7r',
      we: [
        '1559347200',
        '1559433600',
        '1559952000',
        '1560038400',
        '1560556800',
        '1560643200',
        '1561161600',
        '1561248000',
        '1561766400',
        '1561852800',
      ],
    },
  },
  users: {
    '4g42mol1jtfutdaa': {
      beginDate: '1546304400',
      birthday: '978310800',
      email: 'toto@gmail.com',
      firstName: 'Titi',
      id: '4g42mol1jtfutdaa',
      isAdmin: false,
      name: 'Toto',
      password: '4a7d1ed414474e4033ac29ccb8653d9b',
      stopDate: '1576112400',
    },
    z9001bu6sbjtu4er7r: {
      beginDate: '1546304400',
      birthday: '672883200',
      email: 'tutu@gmail.com',
      firstName: 'Titi',
      id: 'z9001bu6sbjtu4er7r',
      isAdmin: true,
      name: 'Tutu',
      password: '4a7d1ed414474e4033ac29ccb8653d9b',
      stopDate: '1588377600',
    },
  },
};

module.exports = defaultDatas;
