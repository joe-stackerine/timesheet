const { getUsers } = require('../services/users');

const findUserByEmail = async emailToFind => {
  try {
    const users = await getUsers();
    return {
      err: null,
      user: Object.values(users).find(({ email }) => email === emailToFind),
    };
  } catch (err) {
    return { err };
  }
};

const findById = async idToFind => {
  try {
    const users = await getUsers();
    return {
      err: null,
      user: Object.values(users).find(({ id }) => id === idToFind),
    };
  } catch (err) {
    return { err };
  }
};

module.exports = { findUserByEmail, findById };
