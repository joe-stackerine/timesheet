const formatProject = project => ({
  ...project,
  users: project.users ? project.users : [],
});

const formatProjects = projects => projects.map(formatProject);

module.exports = { formatProject, formatProjects };
