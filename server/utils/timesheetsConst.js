const TEMP_TIMESHEET = {
  tsId: '',
  userId: '',
  timestamp: '',
  status: 'TODO',
  projects: [{}],
  wd: {},
  md: {},
  dOff: [{}],
  we: [],
};

module.exports = {
  TEMP_TIMESHEET,
};
