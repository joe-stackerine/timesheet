const moment = require('moment');
const uniqid = require('uniqid');
const { getDaysOff } = require('../services/daysOff');
const { getUser, getUsers } = require('../services/users');
const { getProjects } = require('../services/projects');
const { getTimesheets } = require('../services/timesheets');

const verifyTimesheet = async timestamp => {
  const timesheets = Object.values(await getTimesheets());
  const formattedTimestamp = moment(`${timestamp}`, 'X')
    .startOf('Month')
    .add(2, 'hours')
    .format('X');

  return timesheets.find(
    ({ timestamp: timestampsToCompare }) =>
      timestampsToCompare === formattedTimestamp
  );
};

const isWeekEnd = date => date.day() === 0 || date.day() === 6;
const isDayOff = (date, daysOff) =>
  daysOff.find(({ date: dayOffDate }) => dayOffDate === date);

const setWeekends = timestamp =>
  new Array(moment(`${timestamp}`, 'X').daysInMonth())
    .fill(null)
    .map((x, i) => {
      const dayInMonth = moment(`${timestamp}`, 'X')
        .startOf('month')
        .add(i, 'days')
        .add(2, 'hours');
      return isWeekEnd(dayInMonth) ? dayInMonth.format('X') : null;
    })
    .filter(day => !!day);

const setDaysOff = async timestamp => {
  const daysOffList = Object.values(await getDaysOff());
  const daysOff = daysOffList
    .map(({ date, type }) => {
      const isInMonth =
        moment(timestamp, 'X').month() === moment(date, 'X').month();
      const daysOffInfos = isInMonth && { date, type };
      return daysOffInfos;
    })
    .filter(dayOff => !!dayOff);
  return daysOff;
};

const setUsers = async timestamp => {
  const usersList = Object.values(await getUsers());
  const usersId = usersList
    .map(({ beginDate, stopDate, id }) => {
      const isInCompany =
        moment(timestamp, 'X') >= moment(beginDate, 'X') &&
        moment(timestamp, 'X') <= moment(stopDate, 'X');
      const userId = isInCompany && id;
      return userId;
    })
    .filter(user => !!user);
  return usersId;
};

const setProjects = async timestamp => {
  const projectsList = Object.values(await getProjects());
  const projectsInfos = projectsList
    .map(({ beginIn, stopIn, id, projectName, users }) => {
      const isInRun =
        moment(timestamp, 'X').month() >= moment(beginIn, 'X').month() &&
        moment(timestamp, 'X').month() <= moment(stopIn, 'X').month();
      const projectInfos = isInRun && { id, projectName, users };
      return projectInfos;
    })
    .filter(project => !!project);
  return projectsInfos;
};

const getProjectsForUser = (projectsInfos, userId) => {
  const projects = projectsInfos
    .map(({ id: projectId, projectName, users }) => {
      const isInProject = users.find(id => id === userId);
      return !!isInProject && { projectId, projectName };
    })
    .filter(project => !!project);
  return projects;
};

const setPreviewCalendar = async timestamp => {
  const daysOff = await setDaysOff(timestamp);
  const newCalendar = new Array(moment(`${timestamp}`, 'X').daysInMonth())
    .fill(null)
    .map((x, i) => {
      const dayInMonth = moment(`${timestamp}`, 'X')
        .startOf('month')
        .add(i, 'days');
      const formattedDayInMonth = dayInMonth.add(2, 'hours').format('X');
      if (isWeekEnd(dayInMonth)) {
        return { date: formattedDayInMonth, type: 'we' };
      }
      if (isDayOff(formattedDayInMonth, daysOff)) {
        const dayOffType = isDayOff(formattedDayInMonth, daysOff).type;
        return { date: formattedDayInMonth, type: dayOffType };
      }
      return { date: formattedDayInMonth, type: 'wd' };
    });
  return newCalendar;
};

const generateNewTimesheets = (newTempTimesheet, usersId, projectsInfos) => {
  const newTimesheets = usersId.map(userId => {
    const tsId = uniqid();
    const projects = getProjectsForUser(projectsInfos, userId);
    return { ...newTempTimesheet, tsId, userId, projects };
  });
  return newTimesheets;
};

const setTimesheetWithUsersName = async ({
  tsId,
  timestamp,
  status,
  we = [],
  dOff = [],
  wd = {},
  md = {},
  userId,
}) => {
  const nbDaysToFill =
    moment(timestamp, 'X').daysInMonth() - we.length - dOff.length;
    
  const nbFilledDays = Object.values(wd).length + Object.values(md).length;
  const { firstName, name } = await getUser(userId);

  return {
    tsId,
    timestamp,
    status,
    nbDaysToFill,
    nbFilledDays,
    user: { firstName, name },
  };
};


const verifyFilledDays = ({dOff=[], md={},wd={}, we=[], timestamp }) => {
  const nbFilledDays = dOff.length + Object.values(wd).length + Object.values(md).length + we.length;
  const nbDaysInMonth = moment(`${timestamp}`, 'X').daysInMonth();
  return nbFilledDays === nbDaysInMonth;
}

module.exports = {
  verifyTimesheet,
  setPreviewCalendar,
  setWeekends,
  setDaysOff,
  setUsers,
  setProjects,
  generateNewTimesheets,
  setTimesheetWithUsersName,
  verifyFilledDays
};
